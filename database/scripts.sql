USE restapi;

CREATE TABLE usuarios ( id integer PRIMARY KEY AUTO_INCREMENT,  username varchar(255), password varchar(255), nombre varchar(255), apellido varchar(255), avatar varchar(255), email varchar(255), nacimiento DATE);
CREATE TABLE intereses (user_id integer, interes varchar(255));
CREATE TABLE recursos ( id integer PRIMARY KEY AUTO_INCREMENT,  nombre varchar(255), detalle varchar(255), privacidad varchar(255), archivo BLOB);
CREATE TABLE solicitudes (user_id integer, resource_id integer);
CREATE TABLE propiedades (user_id integer, resource_id integer, propiedad varchar(255));
CREATE TABLE invitaciones (user_id integer, resource_id integer, fecha_limite TIMESTAMP);
CREATE TABLE privilegios (user_id integer, resource_id integer, privilegio varchar(255));
