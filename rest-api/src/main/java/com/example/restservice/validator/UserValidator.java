package com.example.restservice.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

@Component("userValidator")
public class UserValidator implements Validator{
	
	private Validator next;
	
	public UserValidator() {
		next = new EmailValidator();
	}

	@Override
	public List<ErrorResponse> validate(User user) {
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		if(user == null) {
			ErrorResponse error = new ErrorResponse();
			error.setDetail("User is required");
			errors.add(error);
		}else{
			errors.addAll(next.validate(user));
		}
		return errors;
	}
	
}
