package com.example.restservice.validator;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

public class NameValidator implements Validator {
	
	private Validator next;
	
	public NameValidator() {
		next = new DateValidator();
	}

	@Override
	public List<ErrorResponse> validate(User user) {
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		if(user.getFirstname() == null || "".equals(user.getFirstname())){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("First Name is required");
			errors.add(error);
		}
		if(user.getSurname() == null || "".equals(user.getSurname())){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("Sur Name is required");
			errors.add(error);
		}
		errors.addAll(next.validate(user));
		return errors;
	}

}
