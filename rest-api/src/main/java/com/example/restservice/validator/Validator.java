package com.example.restservice.validator;

import java.util.List;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

public interface Validator {
	
	public List<ErrorResponse>validate(User user);

}
