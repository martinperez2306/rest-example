package com.example.restservice.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

@Component("credentialsValidator")
public class CredentialsValidator implements Validator {
	
	@Autowired
	@Qualifier("userValidator")
	private Validator next;
	
	public CredentialsValidator() {
		
	}

	@Override
	public List<ErrorResponse> validate(User user) {
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		if(user.getUsername() == null || "".equals(user.getUsername())){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("Username is required");
			errors.add(error);
		}
		if(user.getPassword() == null || "".equals(user.getPassword())){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("Password is required");
			errors.add(error);
		}
		errors.addAll(next.validate(user));
		return errors;
	}

}
