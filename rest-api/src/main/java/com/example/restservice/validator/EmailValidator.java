package com.example.restservice.validator;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

public class EmailValidator implements Validator {
	
	private Validator next;
	public EmailValidator() {
		next = new NameValidator();
	}

	@Override
	public List<ErrorResponse> validate(User user) {
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		if(user.getEmail() == null || "".equals(user.getEmail())){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("Email is required");
			errors.add(error);
		}
		errors.addAll(next.validate(user));
		return errors;
	}

}
