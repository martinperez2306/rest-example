package com.example.restservice.validator;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

public class DateValidator implements Validator {

	public DateValidator() {
		
	}

	@Override
	public List<ErrorResponse> validate(User user) {
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		if(user.getBirthDate() == null){
			ErrorResponse error = new ErrorResponse();
			error.setDetail("Date is required");
			errors.add(error);
		}
		return errors;
	}

}
