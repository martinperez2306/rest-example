package com.example.restservice.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;

@Component
public class ResourceRowMapper implements RowMapper<Resource> {
	
	@Override
	public Resource mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Resource resource = new Resource();
		resource.setId(rs.getLong("ID"));
		resource.setName(rs.getString("NOMBRE"));
		resource.setDetail(rs.getString("DETALLE"));
		switch(ResourcePrivacy.valueOf(rs.getString("PRIVACIDAD"))) {
			case PRIVATE:
				resource.setAsPrivate();
				break;
			case PUBLIC:
				resource.setAsPublic();
				break;
			default:
				break;
		}
		return resource;
	}
}
