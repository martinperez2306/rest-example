package com.example.restservice.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.example.restservice.model.user.User;

@Component
public class UserRowMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		User user = new User();
		user.setId(rs.getLong("ID"));
		user.setAvatar(rs.getString("AVATAR"));
		user.setBirthDate(rs.getDate("NACIMIENTO"));
		user.setEmail(rs.getString("EMAIL"));
		user.setFirstname(rs.getString("NOMBRE"));
		user.setPassword(rs.getString("PASSWORD"));
		user.setSurname(rs.getString("APELLIDO"));
		user.setUsername(rs.getString("USERNAME"));
		return user;
	}
}
