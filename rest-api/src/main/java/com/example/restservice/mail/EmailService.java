package com.example.restservice.mail;

import com.example.restservice.model.user.User;

public interface EmailService {
	
	public void sendSimpleMessage(String to, String subject, String text);
	
	public void sendConfirmAccountMessage(User user);
	
	public void sendConfirmInvitationMessage(String to);

}
