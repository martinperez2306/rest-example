package com.example.restservice.mail;

import org.springframework.mail.SimpleMailMessage;

public class ConfirmAccountMessage {
	
	private SimpleMailMessage simpleMailMessage;
	
	public ConfirmAccountMessage(String to, String from, String path) {
		simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setTo(to);
		simpleMailMessage.setFrom(from);
		simpleMailMessage.setSubject("Confirmacion de Usuario");
		simpleMailMessage.setText("Hola!"
								+ "Gracias por registrarse. Acceda al Link para confirmar su cuenta de usuario.\n"
								+ "\n"
								+ path);
	}
	
	public SimpleMailMessage getMessage() {
		return simpleMailMessage;
	}

}
