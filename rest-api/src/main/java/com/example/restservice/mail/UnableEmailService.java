package com.example.restservice.mail;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.example.restservice.model.user.User;

@Service
@ConditionalOnProperty(value="mail.enabled", havingValue = "false", matchIfMissing = false)
public class UnableEmailService implements EmailService {

	@Override
	public void sendSimpleMessage(String to, String subject, String text) {
		
	}
	
	@Override
	public void sendConfirmAccountMessage(User user) {
		
	}

	@Override
	public void sendConfirmInvitationMessage(String to) {
		
	}

}
