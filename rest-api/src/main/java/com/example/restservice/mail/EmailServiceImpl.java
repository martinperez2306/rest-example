package com.example.restservice.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.restservice.model.user.User;

@Service
@ConditionalOnProperty(value="mail.enabled", havingValue = "true")
public class EmailServiceImpl implements EmailService {

	@Autowired
	private JavaMailSender emailSender;
	
	@Value("${mail.from}")
	private String mailFrom;

	public void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage(); 
		message.setFrom("noreply@mpereztddtest.com");
		message.setTo(to); 
		message.setSubject(subject); 
		message.setText(text);
		emailSender.send(message);
	}

	@Override
	public void sendConfirmAccountMessage(User user) {
		String path = "http://localhost:8080/mails/users/" + user.getId() + "/confirm";
		ConfirmAccountMessage confirmAccountMessage = new ConfirmAccountMessage(user.getEmail(), mailFrom, path);
		emailSender.send(confirmAccountMessage.getMessage());
	}

	@Override
	public void sendConfirmInvitationMessage(String to) {
		// TODO Auto-generated method stub
		
	}
}
