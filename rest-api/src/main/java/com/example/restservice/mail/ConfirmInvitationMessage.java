package com.example.restservice.mail;

import org.springframework.mail.SimpleMailMessage;

public class ConfirmInvitationMessage {
	
	private SimpleMailMessage simpleMailMessage;
	
	public ConfirmInvitationMessage(String to, String from, String path) {
		simpleMailMessage.setTo(to);
		simpleMailMessage.setFrom(from);
		simpleMailMessage.setSubject("Confirmacion de Invitacion");
		simpleMailMessage.setText("Se ha invitado acceder a un recurso\n"
								+ "Ingrese al siguiente link para acpetar la invitacion\n"
								+ "\n"
								+ path);
	}
	
	public SimpleMailMessage getMessage() {
		return simpleMailMessage;
	}

}
