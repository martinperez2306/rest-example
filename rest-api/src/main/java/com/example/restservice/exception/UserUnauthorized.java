package com.example.restservice.exception;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;

import com.example.restservice.model.response.ErrorResponse;

public class UserUnauthorized extends ApiException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6152637604831534536L;

	public UserUnauthorized() {
		super(HttpStatus.UNAUTHORIZED, new ArrayList<ErrorResponse>());
	}

}
