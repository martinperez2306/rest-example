package com.example.restservice.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.example.restservice.model.response.ErrorResponse;

public class ApiException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3319599436931852463L;
	
	
	private HttpStatus status;
	private List<ErrorResponse> errors;
	
	public ApiException() {
		status = HttpStatus.INTERNAL_SERVER_ERROR;
		errors = new ArrayList<ErrorResponse>();
	}
	
	public ApiException(HttpStatus status, List<ErrorResponse> errors) {
		this.status = status;
		this.errors = errors;
	}
	
	public HttpStatus getStatus() {
		return status;
	}
	
	public List<ErrorResponse> getErrors(){
		return errors;
	}
	
	protected void addError(ErrorResponse error) {
		this.errors.add(error);
	}
	
}
