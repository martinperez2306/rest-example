package com.example.restservice.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.example.restservice.model.response.ErrorResponse;

public class UserBadRequestException extends ApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3797881233185013232L;
	
	public UserBadRequestException() {
		super(HttpStatus.BAD_REQUEST, new ArrayList<ErrorResponse>());
	}
	
	public UserBadRequestException(List<ErrorResponse> errors) {
		super(HttpStatus.BAD_REQUEST, errors);
	}
	
}
