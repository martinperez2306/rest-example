package com.example.restservice.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.example.restservice.model.response.ErrorResponse;

public class ForbidenException extends ApiException {

	private static final long serialVersionUID = -5136656564388565839L;
	
	public ForbidenException() {
		super(HttpStatus.FORBIDDEN, new ArrayList<ErrorResponse>());
	}
	
	public ForbidenException(List<ErrorResponse> errors) {
		super(HttpStatus.FORBIDDEN, errors);
	}

}
