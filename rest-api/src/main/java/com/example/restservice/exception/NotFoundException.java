package com.example.restservice.exception;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;

import com.example.restservice.model.response.ErrorResponse;

public class NotFoundException extends ApiException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8929511943112362263L;
	
	public NotFoundException() {
		super(HttpStatus.NOT_FOUND, new ArrayList<ErrorResponse>());
		ErrorResponse error = new ErrorResponse();
		error.setDetail("Not Found");
		addError(error);
	}
	
}
