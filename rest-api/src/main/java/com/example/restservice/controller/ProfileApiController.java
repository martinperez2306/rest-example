package com.example.restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.restservice.adapter.UserAdapter;
import com.example.restservice.api.ProfileApi;
import com.example.restservice.exception.ApiException;
import com.example.restservice.model.response.UserResponse;
import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRestInput;
import com.example.restservice.service.ProfileService;
import com.example.restservice.utils.Constants;

@Controller
public class ProfileApiController implements ProfileApi {
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private UserAdapter userAdapter;

	@Override
	public ResponseEntity<UserResponse> getProfile(@RequestHeader("Authorization") String token) {
		UserResponse response = new UserResponse();
		try {
			User user = profileService.getUserProfile(token);
			response.setData(userAdapter.adaptToRestWithPrivateData(user));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<UserResponse> updateProfile(@RequestHeader("Authorization") String token, UserRestInput userRestInput) {
		UserResponse response = new UserResponse();
		try {
			User user = profileService.getUserProfile(token);
			User userUpdated = userAdapter.adaptToModel(userRestInput);
			profileService.updateUserProfile(user, userUpdated);
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}
	}

}
