package com.example.restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.example.restservice.api.LoginApi;
import com.example.restservice.auth.UserCredentials;
import com.example.restservice.exception.ApiException;
import com.example.restservice.model.response.LoginResponse;
import com.example.restservice.service.LoginService;
import com.example.restservice.utils.Constants;

@Controller
public class LoginApiController implements LoginApi{
	
	@Autowired
	private LoginService loginService;

	@Override
	public ResponseEntity<LoginResponse> login(UserCredentials credentials) {
		LoginResponse response = new LoginResponse();
		try {
			String token = loginService.login(credentials);
			response.setToken(token);		
			return new ResponseEntity<LoginResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<LoginResponse>(response, e.getStatus());
		}
	}

}
