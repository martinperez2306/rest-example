package com.example.restservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.example.restservice.api.MailApi;
import com.example.restservice.exception.ApiException;
import com.example.restservice.model.response.UserResponse;
import com.example.restservice.service.UserService;
import com.example.restservice.utils.Constants;

@Controller
public class MailApiController implements MailApi {
	
	@Autowired
	private UserService userService;

	@Override
	public ResponseEntity<UserResponse> confirmUser(Long userId) {
		UserResponse response = new UserResponse();
		try {
			userService.confirmUser(userId);
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}
	}

}
