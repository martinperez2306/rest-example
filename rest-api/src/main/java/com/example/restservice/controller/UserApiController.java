package com.example.restservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.example.restservice.adapter.RegisterAdapter;
import com.example.restservice.adapter.UserAdapter;
import com.example.restservice.api.UserApi;
import com.example.restservice.exception.ApiException;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.response.UserCollectionResponse;
import com.example.restservice.model.response.UserResponse;
import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRegister;
import com.example.restservice.service.ResourceService;
import com.example.restservice.service.TokenService;
import com.example.restservice.service.UserService;
import com.example.restservice.utils.Constants;

@Controller
public class UserApiController implements UserApi{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserAdapter userAdapter;
	
	@Autowired
	private RegisterAdapter registerAdapter;
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private TokenService tokenService;

	@Override
	public ResponseEntity<UserCollectionResponse> getUsers() {
		UserCollectionResponse response = new UserCollectionResponse();
		try {
			List<User> users = userService.getUsers();
			response.setData(userAdapter.adaptToRest(users));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserCollectionResponse>(response, HttpStatus.OK);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserCollectionResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<UserResponse> getUser(Long userId) {
		UserResponse response = new UserResponse();
		try {
			User user = userService.getUser(userId);
			response.setData(userAdapter.adaptToRest(user));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<UserResponse> addUser(UserRegister user) {
		try {
			User userCreated = userService.addUser(registerAdapter.adaptToModel(user));
			HttpHeaders headers = new HttpHeaders();
			headers.add("Location", "/user/"+userCreated.getId());
			headers.add("Content-Location", userCreated.getId().toString());
			return new ResponseEntity<UserResponse>(headers, HttpStatus.CREATED);
		}catch(ApiException e) {
			UserResponse response = new UserResponse();
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}		
	}

	@Override
	public ResponseEntity<UserResponse> deleteUser(Long userId) {
		try {
			userService.deleteUser(userId);
			return new ResponseEntity<UserResponse>(HttpStatus.NO_CONTENT);
		}catch(ApiException e) {
			UserResponse response = new UserResponse();
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserResponse>(response, e.getStatus());
		}	
	}

	@Override
	public ResponseEntity<UserCollectionResponse> getUserOfResource(String token, Long resourceId) {
		UserCollectionResponse response = new UserCollectionResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			Resource resource = resourceService.getResource(resourceId, owner.getId());
			List<User> users = userService.getUsersOfResource(resource.getId());
			response.setData(userAdapter.adaptToRest(users));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<UserCollectionResponse>(response, HttpStatus.OK);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<UserCollectionResponse>(response, e.getStatus());
		}
	}

}
