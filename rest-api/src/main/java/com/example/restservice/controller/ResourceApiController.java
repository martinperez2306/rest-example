package com.example.restservice.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.example.restservice.adapter.ResourceAdapter;
import com.example.restservice.adapter.ResourceOwnershipAdapter;
import com.example.restservice.api.ResourceApi;
import com.example.restservice.exception.ApiException;
import com.example.restservice.model.access.AccessRequest;
import com.example.restservice.model.access.AccessRequestRestData;
import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.invitation.InvitationRestData;
import com.example.restservice.model.invitation.InvitationRestInput;
import com.example.restservice.model.privileges.NoPrivilege;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ReadPrivilge;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.privileges.WritePrivilege;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourceRestInput;
import com.example.restservice.model.response.AccessRequestCollectionResponse;
import com.example.restservice.model.response.AccessRequestResponse;
import com.example.restservice.model.response.InvitationCollectionResponse;
import com.example.restservice.model.response.ResourceCollectionResponse;
import com.example.restservice.model.response.ResourceResponse;
import com.example.restservice.model.user.User;
import com.example.restservice.service.FileService;
import com.example.restservice.service.ResourceService;
import com.example.restservice.service.TokenService;
import com.example.restservice.utils.Constants;

@Controller
public class ResourceApiController implements ResourceApi {
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private ResourceAdapter resourceAdapter;
	
	@Autowired
	private ResourceOwnershipAdapter resourceOwnershipAdapter;

	@Override
	public ResponseEntity<ResourceCollectionResponse> getPublicResources() {
		ResourceCollectionResponse response = new ResourceCollectionResponse();
		try {
			List<Resource> resources = resourceService.getPublicResources();
			response.setData(resourceAdapter.adaptToRest(resources));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<ResourceCollectionResponse>(response, HttpStatus.OK);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceCollectionResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> getResource(String token, Long resourceId) {
		ResourceResponse response = new ResourceResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			Resource resource = resourceService.getResource(resourceId, user.getId());
			//TODO: Validar que el usuario pueda acceder al recurso -> si no lanzar 403
			response.setData(resourceAdapter.adaptToRest(resource));
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> addResource(String token, ResourceRestInput restInput, MultipartFile file) {
		ResourceResponse response = new ResourceResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			Resource resource = resourceAdapter.adaptToModel(restInput);
			Resource resourceCreated = resourceService.createResource(resource, user);
			fileService.uploadResourceFile(resourceCreated, file);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Location", "/resources/"+resourceCreated.getId());
			headers.add("Content-Location", resourceCreated.getId().toString());
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<ResourceResponse>(headers, HttpStatus.CREATED);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> updateResource(String token, Long resourceId, ResourceRestInput resource, MultipartFile file) {
		ResourceResponse response = new ResourceResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			Resource finded = resourceService.getResource(resourceId, user.getId());
			resourceService.modifyResource(finded.getId(), user.getId(), resourceAdapter.adaptToModel(resource));
			fileService.uploadResourceFile(finded, file);
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.NO_CONTENT);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> deleteResource(String token, Long resourceId) {
		ResourceResponse response = new ResourceResponse();
		try {
			//TODO: Validar que el usuario pueda eliminar al recurso -> si no lanzar 403
			User user = tokenService.getUserWithToken(token);
			resourceService.deleteResource(resourceId, user.getId());
			response.setStatus(Constants.SUCCESS_RESPONSE);
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.NO_CONTENT);
		}catch(ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}
	
	@Override
	public ResponseEntity<byte[]> downloadResourceFile(String token, Long resourceId) {
		try {
			User owner = tokenService.getUserWithToken(token);
			Resource resource = resourceService.getResource(resourceId, owner.getId());
			byte[] bytes = fileService.downloadResourceFile(resource.getId());
			String fileName = resource.getName();
			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.setContentLength(bytes.length);
			respHeaders.setContentType(new MediaType("text", "json"));
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
			return new ResponseEntity<byte[]>(bytes, respHeaders, HttpStatus.OK);
		}catch(ApiException e) {
			return new ResponseEntity<byte[]>(e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceCollectionResponse> getUserResources(String token) {
		ResourceCollectionResponse response = new ResourceCollectionResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			List<ResourceOwnership> resourceOwnerships = resourceService.getResourcesForUser(user.getId());
			response.setData(resourceOwnershipAdapter.adaptToRest(resourceOwnerships));
			return new ResponseEntity<ResourceCollectionResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceCollectionResponse>(response, e.getStatus());
		}
		
	}

	@Override
	public ResponseEntity<AccessRequestResponse> requestAccessToResource(String token, Long resourceId) {
		AccessRequestResponse response = new AccessRequestResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			resourceService.requestResourceAccess(resourceId, user.getId());
			return new ResponseEntity<AccessRequestResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<AccessRequestResponse>(response, e.getStatus());
		}
	}
	
	@Override
	public ResponseEntity<AccessRequestCollectionResponse> getAccessRequestsToResoruce(String token) {
		AccessRequestCollectionResponse response = new AccessRequestCollectionResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			//TODO: Adaptar
			List<AccessRequest> requests = resourceService.getRequestedResources(owner.getId());
			List<AccessRequestRestData> restData = requests.stream().map(r -> new AccessRequestRestData(r.getUserId(), r.getResourceId())).collect(Collectors.toList());
			response.setData(restData);
			return new ResponseEntity<AccessRequestCollectionResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<AccessRequestCollectionResponse>(response, e.getStatus());
		}
	}
	
	@Override
	public ResponseEntity<AccessRequestResponse> acceptAccessToResource(String token, Long resourceId, Long userId) {
		AccessRequestResponse response = new AccessRequestResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			resourceService.inviteUserToResource(userId, resourceId);
			return new ResponseEntity<AccessRequestResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<AccessRequestResponse>(response, e.getStatus());
		}
	}
	
	@Override
	public ResponseEntity<AccessRequestResponse> rejectAccessToResource(String token, Long resourceId, Long userId) {
		AccessRequestResponse response = new AccessRequestResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			resourceService.forbidResourceToUser(resourceId, owner.getId(), userId);
			return new ResponseEntity<AccessRequestResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<AccessRequestResponse>(response, e.getStatus());
		}
	}
	
	@Override
	public ResponseEntity<InvitationCollectionResponse> getInvitations(String token) {
		InvitationCollectionResponse response = new InvitationCollectionResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			//TODO: Adaptar
			List<Invitation> invitations = resourceService.getResourceInvitations(user.getId());
			List<InvitationRestData> restData = invitations.stream().map(i -> new InvitationRestData(i.getUserId(), i.getResourceId(), i.getInvitationCode())).collect(Collectors.toList());
			response.setData(restData);
			return new ResponseEntity<InvitationCollectionResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<InvitationCollectionResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> inviteUserToResource(String token, Long resourceId, Long userId, InvitationRestInput invitation) {
		ResourceResponse response = new ResourceResponse();
		try {
			User user = tokenService.getUserWithToken(token);
			//TODO: Validar que el owner puede invitar
			if(invitation.getActions() != null && !invitation.getActions().isEmpty()) {
				List<Privilege> privileges = invitation.getActions().stream().map(a -> this.fromAction(a)).collect(Collectors.toList());
				resourceService.inviteUserToResource(userId, resourceId, privileges);
			}else {
				resourceService.inviteUserToResource(userId, resourceId);
			}
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}
	
	//TODO: Encontrar una forma mejor
	private Privilege fromAction(String action) {
		switch(action) {
			case "READ":
				return new ReadPrivilge();
			case "WRITE":
				return new WritePrivilege();
			default:
				return new NoPrivilege();
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> shareResource(String token, Long resourceId, Long userId) {
		ResourceResponse response = new ResourceResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			//TODO: Validar que el usuario pueda compartir ese recurso al otro usuario
			resourceService.shareResourceToUser(resourceId, owner.getId(), userId);
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}

	@Override
	public ResponseEntity<ResourceResponse> forbidResource(String token, Long resourceId, Long userId) {
		ResourceResponse response = new ResourceResponse();
		try {
			User owner = tokenService.getUserWithToken(token);
			//TODO: Validar que el usuario pueda compartir ese recurso al otro usuario
			resourceService.forbidResourceToUser(resourceId, owner.getId(), userId);
			return new ResponseEntity<ResourceResponse>(response, HttpStatus.OK);
		} catch (ApiException e) {
			response.setStatus(Constants.FAILURE_RESPONSE);
			response.setErrors(e.getErrors());
			return new ResponseEntity<ResourceResponse>(response, e.getStatus());
		}
	}

}
