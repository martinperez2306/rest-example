package com.example.restservice.utils;

public class Constants {
	
	public static final String SUCCESS_RESPONSE = "SUCCESS";
	public static final String FAILURE_RESPONSE = "FAILURE";
	
	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String PREFIX_AUTHORIZATION = "Bearer ";
	public static final String SECRET_KEY = "mySecretKey";

}
