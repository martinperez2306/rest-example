package com.example.restservice.service;

import org.springframework.web.multipart.MultipartFile;

import com.example.restservice.model.resource.Resource;

public interface FileService {
	
	public void uploadResourceFile(Resource resource, MultipartFile file);
	
	public byte[] downloadResourceFile(Long resourceId);
	
}
