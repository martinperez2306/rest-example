package com.example.restservice.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.example.restservice.auth.UserCredentials;
import com.example.restservice.exception.ApiException;
import com.example.restservice.exception.UserUnauthorized;
import com.example.restservice.model.user.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenService tokenService;
	
	@Value("${session.timeout:600000}")
	private Long sessionTimeOut;

	
	@Override
	public String login(UserCredentials credentials) throws ApiException{
		User user = userService.getUserByUsername(credentials.getUsername());
		if(userNotFound(user) || invalidCredentials(credentials.getPassword(), user))
			throw new UserUnauthorized();
		String token = this.getJWTToken(credentials);
		tokenService.saveTokenForUser(token, user.getId());
		return token;
	}

	public String getJWTToken(UserCredentials credentials) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(credentials.getUsername())
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + sessionTimeOut))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
	
	private Boolean userNotFound(User user) {
		return (user == null || user.getId() == null || user.getId() <= 0);
	}
	
	private Boolean invalidCredentials(String password, User user) {
		return !(user.getPassword().equals(password));
	}

	

}
