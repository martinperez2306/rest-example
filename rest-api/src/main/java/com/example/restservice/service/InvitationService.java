package com.example.restservice.service;

import java.util.List;

import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;

public interface InvitationService {

	public void inviteUserToResource(User user, Resource resource);
	
	public void inviteUserToResource(User user, Resource resource, List<Privilege> privileges);

	public List<Invitation> getResourceInvitations(Long userId);

	public void removeResourceInvitation(Long userId, Long resourceId);
	
	public Invitation getInvitation(Long userId, Long resourceId);

}
