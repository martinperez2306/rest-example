package com.example.restservice.service;

import java.util.List;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.user.User;

public interface UserService {
	
	public List<User> getUsers() throws ApiException;
	
	public User getUser(Long userId) throws ApiException;
	
	public User addUser(User user) throws ApiException;
	
	public User updateUser(Long userId, User user) throws ApiException;
	
	public User modifyUser(Long userId, User user) throws ApiException;
	
	public void deleteUser(Long userId) throws ApiException;
	
	public User getUserByUsername(String username) throws ApiException;
	
	public void confirmUser(Long userId) throws ApiException;
	
	public List<User> getUsersOfResource(Long resourceId) throws ApiException;
}
