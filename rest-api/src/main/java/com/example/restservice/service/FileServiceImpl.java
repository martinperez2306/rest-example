package com.example.restservice.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.repository.ResourceFileRepository;

@Service
public class FileServiceImpl implements FileService {
	
	@Autowired
	private ResourceFileRepository resourceFileRepository;

	@Override
	public void uploadResourceFile(Resource resource, MultipartFile file) {
		try {
			resourceFileRepository.update(resource.getId(), file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public byte[] downloadResourceFile(Long resourceId) {
		return resourceFileRepository.select(resourceId);
	}

}
