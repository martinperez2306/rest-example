package com.example.restservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.exception.UserBadRequestException;
import com.example.restservice.mail.EmailService;
import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;
import com.example.restservice.repository.ResourceOwnershipRepository;
import com.example.restservice.repository.UserRepository;
import com.example.restservice.validator.Validator;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	@Qualifier("userValidator")
	private Validator userValidator;
	
	@Autowired
	@Qualifier("credentialsValidator")
	private Validator credentialsValidator;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private ResourceOwnershipRepository resourceOwnershipRepository;

	@Override
	public List<User> getUsers() throws ApiException{
		List<User> users = new ArrayList<User>();
		users.addAll(userRepository.select());
		return users;
	}

	@Override
	public User getUser(Long userId) throws ApiException {
		validateUserId(userId);
		User user = userRepository.select(userId);
		if(userNotFound(user))
			throw new NotFoundException();
		return user;
	}

	@Override
	public User addUser(User user) throws ApiException{
		List<ErrorResponse> errors = credentialsValidator.validate(user);
		if(!errors.isEmpty())
			throw new UserBadRequestException(errors);
		User created = userRepository.insert(user);
		emailService.sendConfirmAccountMessage(user);
		return created;
	}

	@Override
	public User updateUser(Long userId, User user) throws ApiException{
		validateUserId(userId);
		User oldUser = userRepository.select(userId);
		if(userNotFound(oldUser))
			throw new NotFoundException();
		List<ErrorResponse> errors = userValidator.validate(user);
		if(!errors.isEmpty())
			throw new UserBadRequestException(errors);
		return userRepository.update(userId, user);
	}
	
	@Override
	public User modifyUser(Long userId, User user) throws ApiException{
		validateUserId(userId);
		User oldUser = userRepository.select(userId);
		if(userNotFound(oldUser))
			throw new NotFoundException();
		return userRepository.update(userId, user);
	}

	@Override
	public void deleteUser(Long userId) throws ApiException{
		validateUserId(userId);
		User userToDelete = userRepository.select(userId);
		if(userNotFound(userToDelete))
			throw new NotFoundException();
		userRepository.delete(userId);
	}
	
	@Override
	public User getUserByUsername(String username) throws ApiException {
		User user = userRepository.selectByUsername(username);
		return user;
	}
	
	@Override
	public void confirmUser(Long userId) throws ApiException{
		User user = userRepository.select(userId);
		user.confirmAccont();
		userRepository.update(userId, user);
	}

	private void validateUserId(Long userId) throws ApiException {
		if(userId == null)
			throw new UserBadRequestException();
	}
	
	private Boolean userNotFound(User user) {
		return (user == null || user.getId() == null || user.getId() <= 0);
	}

	@Override
	public List<User> getUsersOfResource(Long resourceId) throws ApiException {
		return resourceOwnershipRepository.selectByUser(resourceId).stream().map(ro -> ro.getUser()).collect(Collectors.toList());
	}

}
