package com.example.restservice.service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.user.User;

public interface ProfileService {
	
	public User getUserProfile(String token) throws ApiException;
	
	public void updateUserProfile(User oldUserProfile, User newUserProfile) throws ApiException;

}
