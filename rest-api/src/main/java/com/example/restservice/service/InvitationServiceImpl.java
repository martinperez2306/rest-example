package com.example.restservice.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;
import com.example.restservice.repository.InvitationRepository;

@Service
public class InvitationServiceImpl implements InvitationService {
	
	@Autowired
	private InvitationRepository invitationRepository;
	
	@Value("${invitation.expiration.limit:604800000}")
	private Long limitTime;
	
	@Override
	public void inviteUserToResource(User user, Resource resource) {
		Invitation invitation = new Invitation(user.getId(), resource.getId(), new Date(System.currentTimeMillis() + limitTime));
		invitationRepository.insert(invitation);
	}
	
	@Override
	public void inviteUserToResource(User user, Resource resource, List<Privilege> privileges) {
		Invitation invitation = new Invitation(user.getId(), resource.getId(), new Date(System.currentTimeMillis() + limitTime), privileges);
		invitationRepository.insert(invitation);
	}

	@Override
	public List<Invitation> getResourceInvitations(Long userId) {
		return invitationRepository.selectByUser(userId);
	}

	@Override
	public void removeResourceInvitation(Long userId, Long resourceId) {
		invitationRepository.delete(userId, resourceId);
	}

	@Override
	public Invitation getInvitation(Long userId, Long resourceId) {
		return invitationRepository.select(userId, resourceId);
	}

}
