package com.example.restservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.user.User;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private UserService userService;
	
	@Override
	public User getUserProfile(String token) throws ApiException {
		return tokenService.getUserWithToken(token);
	}

	@Override
	public void updateUserProfile(User oldUserProfile, User newUserProfile) throws ApiException {
		userService.updateUser(oldUserProfile.getId(), newUserProfile);
	}
	
}
