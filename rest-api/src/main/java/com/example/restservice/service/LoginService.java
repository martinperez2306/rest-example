package com.example.restservice.service;

import com.example.restservice.auth.UserCredentials;
import com.example.restservice.exception.ApiException;

public interface LoginService {
	
	public String login(UserCredentials credentials) throws ApiException;
	
}
