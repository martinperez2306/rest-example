package com.example.restservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.user.User;
import com.example.restservice.repository.TokenRepository;

@Service
public class TokenServiceImpl implements TokenService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenRepository tokenRepository;

	@Override
	public void saveTokenForUser(String token, Long idUser) {
		try {
			User user = userService.getUser(idUser);
			tokenRepository.insertTokenForUser(token, idUser);
		} catch (ApiException e) {
			e.printStackTrace();
		}
	}

	@Override
	public User getUserWithToken(String token) throws ApiException {
		Long userId = tokenRepository.selectUserWithToken(token);
		return userService.getUser(userId);
	}

}
