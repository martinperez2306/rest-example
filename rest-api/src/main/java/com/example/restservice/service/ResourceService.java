package com.example.restservice.service;

import java.util.List;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.access.AccessRequest;
import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;

public interface ResourceService {
	
	public List<Resource> getPublicResources() throws ApiException;
	
	public Resource createResource(Resource resource, User owner) throws ApiException;
	
	public Resource getResource(Long resourceId, Long authorizedUserId) throws ApiException;
	
	public void modifyResource(Long resourceId, Long authorizedUserId, Resource resourceUpdated) throws ApiException;
	
	public void deleteResource(Long resourceId, Long ownerId) throws ApiException;
	
	public List<ResourceOwnership> getResourcesForUser(Long userId) throws ApiException;
	
	public void requestResourceAccess(Long resourceId, Long requestingUserId) throws ApiException;
	
	public List<AccessRequest> getRequestedResources(Long ownerId) throws ApiException;
	
	public void deniedAccessRequest(Long userId, Long resourceId);
	
	public List<Invitation> getResourceInvitations(Long userId) throws ApiException;
	
	public Long inviteUserToResource(Long userId, Long resourceId) throws ApiException;
	
	public Long inviteUserToResource(Long userId, Long resourceId, List<Privilege> privileges) throws ApiException;
	
	public void removeResourceInvitationToUser(Long resourceId, Long userId) throws ApiException;

	public void shareResourceToUser(Long resourceId, Long userOwnerId, Long userId) throws ApiException;

	public void forbidResourceToUser(Long resourceId, Long userOwnerId, Long userId) throws ApiException;
	
}
