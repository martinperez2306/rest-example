package com.example.restservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.model.access.AccessRequest;
import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.privileges.Invited;
import com.example.restservice.model.privileges.Owner;
import com.example.restservice.model.privileges.Ownership;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;
import com.example.restservice.model.user.User;
import com.example.restservice.repository.AccessRequestRepository;
import com.example.restservice.repository.ResourceOwnershipRepository;
import com.example.restservice.repository.ResourceRepository;

@Service
public class ResourceServiceImpl implements ResourceService{
	
	@Autowired
	private ResourceRepository resourceRepository;
	
	@Autowired
	private ResourceOwnershipRepository ownershipRepository;
	
	@Autowired
	private AccessRequestRepository accessRequestRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private InvitationService invitationService;

	@Override
	public Resource createResource(Resource resource, User owner) throws ApiException{
		resource = resourceRepository.insert(resource);
		ResourceOwnership ownership = new ResourceOwnership(owner, resource, new Owner());
		ownershipRepository.insert(ownership);
		return resource;
	}
	
	@Override
	public List<Resource> getPublicResources() throws ApiException{
		return resourceRepository.selectByPrivacy(ResourcePrivacy.PUBLIC);
	}

	@Override
	public Resource getResource(Long resourceId, Long authorizedUserId) throws ApiException{
		Resource resource = resourceRepository.select(resourceId);
		if(resource.getId() == null)
			throw new NotFoundException();
		ResourceOwnership resourceOwnership = ownershipRepository.select(authorizedUserId, resourceId);
		return resourceOwnership.readResource();
	}

	@Override
	public void modifyResource(Long resourceId, Long authorizedUserId, Resource resourceUpdated) throws ApiException{
		ResourceOwnership resourceOwnership = ownershipRepository.select(authorizedUserId, resourceId);
		Resource update = resourceOwnership.writeResource(resourceUpdated);
		resourceRepository.update(resourceId, update);
	}

	@Override
	public void deleteResource(Long resourceId, Long ownerId) throws ApiException{
		ResourceOwnership resourceOwnership = ownershipRepository.select(ownerId, resourceId);
		resourceOwnership.deleteResource();
		ownershipRepository.deleteByResource(resourceId);
		resourceRepository.delete(resourceId);
	}

	@Override
	public List<ResourceOwnership> getResourcesForUser(Long userId) throws ApiException{
		return ownershipRepository.selectByUser(userId).stream().collect(Collectors.toList());
	}

	@Override
	public void requestResourceAccess(Long resourceId, Long requestingUserId) throws ApiException {
		User user = userService.getUser(requestingUserId);
		Resource resource = resourceRepository.select(resourceId);
		AccessRequest accessRequest = new AccessRequest();
		accessRequest.setUserId(requestingUserId);
		accessRequest.setResourceId(resourceId);
		accessRequestRepository.insert(accessRequest);
	}

	@Override
	public List<AccessRequest> getRequestedResources(Long ownerId) throws ApiException {
		List<Resource> resources = this.getResourcesForUser(ownerId).stream().map(ro -> ro.getResource()).collect(Collectors.toList());
		List<AccessRequest> requests = new ArrayList<AccessRequest>();
		resources.forEach(r -> requests.addAll(accessRequestRepository.selectByResource(r.getId())));
		return requests;
	}
	
	@Override
	public void deniedAccessRequest(Long userId, Long resourceId) {
		//TODO: Validar que no pueda borrar si esto est� en estado Invitacion
		accessRequestRepository.delete(userId, resourceId);
	}

	@Override
	public List<Invitation> getResourceInvitations(Long userId) throws ApiException {
		return invitationService.getResourceInvitations(userId);
	}

	@Override
	public Long inviteUserToResource(Long userId, Long resourceId) throws ApiException {
		User user = userService.getUser(userId);
		//TODO: Tal vez usar el servicio, de paso valida si el usuario puede invitar o no
		Resource resource = resourceRepository.select(resourceId);
		accessRequestRepository.delete(userId, resourceId);
		invitationService.inviteUserToResource(user, resource);
		return 0L;//TODO: Devolver el token?
	}
	
	@Override
	public Long inviteUserToResource(Long userId, Long resourceId, List<Privilege> privileges) throws ApiException {
		User user = userService.getUser(userId);
		//TODO: Tal vez usar el servicio, de paso valida si el usuario puede invitar o no
		Resource resource = resourceRepository.select(resourceId);
		accessRequestRepository.delete(userId, resourceId);
		invitationService.inviteUserToResource(user, resource, privileges);
		return 0L;
	}
	
	@Override
	public void removeResourceInvitationToUser(Long resourceId, Long userId) throws ApiException {
		//TODO: Validar que no pueda borrar si esto est� en estado Invitado, Propietario
		invitationService.removeResourceInvitation(userId, resourceId);
	}
	
	@Override
	public void shareResourceToUser(Long resourceId, Long userOwnerId, Long userId) throws ApiException {
		User user = userService.getUser(userId);
		Resource resource = this.resourceRepository.select(resourceId);
		Invitation invitation = invitationService.getInvitation(userId, resourceId);
		Ownership invite = new Invited(invitation.getPrivileges());
		ResourceOwnership resourceOwnership = new ResourceOwnership(user, resource, invite);
		invitationService.removeResourceInvitation(userId, resourceId);
		ownershipRepository.insert(resourceOwnership);
	}

	@Override
	public void forbidResourceToUser(Long resourceId, Long userOwnerId, Long userId) throws ApiException {
		ownershipRepository.delete(userId, resourceId);
	}

}
