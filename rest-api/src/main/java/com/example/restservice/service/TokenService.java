package com.example.restservice.service;

import com.example.restservice.exception.ApiException;
import com.example.restservice.model.user.User;

public interface TokenService {
	
	public void saveTokenForUser(String token, Long idUser);
	
	public User getUserWithToken(String token) throws ApiException;

}
