package com.example.restservice.repository;

public interface TokenRepository {
	
	public void insertTokenForUser(String token, Long userId);
	
	public Long selectUserWithToken(String token);

	public void deleteTokenUser(String token);
}
