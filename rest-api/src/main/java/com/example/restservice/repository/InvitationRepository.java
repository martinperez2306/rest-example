package com.example.restservice.repository;

import java.util.List;

import com.example.restservice.model.invitation.Invitation;

public interface InvitationRepository {
	
	public Invitation insert(Invitation invitation);
	
	public Invitation select(Long userId, Long resourceId);
	
	public List<Invitation> selectByUser(Long userId);

	public void delete(Long userId, Long resourceId);
	
	public void deleteExpiredInvitations();

}
