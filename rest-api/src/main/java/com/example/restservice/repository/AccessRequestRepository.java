package com.example.restservice.repository;

import java.util.List;

import com.example.restservice.model.access.AccessRequest;

public interface AccessRequestRepository {
	
	public AccessRequest insert(AccessRequest request);
	
	public AccessRequest select(Long userId, Long resourceId);
	
	public List<AccessRequest> selectByUser(Long userId);
	
	public List<AccessRequest> selectByResource(Long resourceId); 

	public void delete(Long userId, Long resourceId);

}
