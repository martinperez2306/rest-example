package com.example.restservice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.privileges.Invited;
import com.example.restservice.model.privileges.NoOwnership;
import com.example.restservice.model.privileges.NoPrivilege;
import com.example.restservice.model.privileges.Owner;
import com.example.restservice.model.privileges.Ownership;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ReadPrivilge;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.privileges.WritePrivilege;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourceOwnershipEnum;
import com.example.restservice.model.user.User;
import com.example.restservice.rowmapper.ResourceRowMapper;
import com.example.restservice.rowmapper.UserRowMapper;

@Repository	
@ConditionalOnProperty(value="repository.access", havingValue = "dao")
public class ResourceOwnershipDao implements ResourceOwnershipRepository, InitializingBean{
	
	private final static String USER_TABLE = "usuarios";
	private final static String RESOURCE_TABLE = "recursos";
	
	private final static String OWNERSHIP_TABLE = "propiedades";
	private final static String PRIVILEGE_TABLE = "privilegios";
	
	private final static String SELECT_OWNERSHIPS = "SELECT * FROM "+ OWNERSHIP_TABLE + " AS O, " + RESOURCE_TABLE + " AS R, "+ USER_TABLE + " AS U, "+PRIVILEGE_TABLE+" AS P WHERE U.ID = O.USER_ID AND R.ID = O.RESOURCE_ID AND P.USER_ID = O.USER_ID AND P.RESOURCE_ID = O.RESOURCE_ID";
	private final static String SELECT_OWNERSHIP = SELECT_OWNERSHIPS + " AND O.USER_ID = :user_id AND O.RESOURCE_ID = :resource_id";
	private final static String INSERT_OWNERSHIP = "INSERT INTO "+ OWNERSHIP_TABLE +" (USER_ID, RESOURCE_ID, PROPIEDAD) VALUES (:user_id, :resource_id, :propiedad)";
	private final static String UPDATE_OWNERSHIP = "UPDATE "+ OWNERSHIP_TABLE +" SET PROPIEDAD = :propiedad WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	private final static String DELETE_OWNERSHIP = "DELETE FROM "+ OWNERSHIP_TABLE +" WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	
	private final static String SELECT_OWNERSHIPS_BY_USER = SELECT_OWNERSHIPS + " AND O.USER_ID = :user_id";
	private final static String SELECT_OWNERSHIPS_BY_RESOURCE = SELECT_OWNERSHIPS + " AND O.RESOURCE_ID = :resource_id";
	
	private final static String INSERT_PRIVILEGE = "INSERT INTO "+ PRIVILEGE_TABLE +" (USER_ID, RESOURCE_ID, PRIVILEGIO) VALUES (:user_id, :resource_id, :privilegio)";
	private final static String DELETE_OWNERSHIP_PRIVILEGES = "DELETE FROM "+ PRIVILEGE_TABLE +" WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private ResourceRowMapper resourceRowMapper;
	
	@Autowired
	private UserRowMapper userRowMapper;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private ResourceOwnershipRowMapper resourceOwnershipRowMapper;
	
	private class ResourceOwnershipRowMapper implements RowMapper<ResourceOwnership> {
		@Override
		public ResourceOwnership mapRow(ResultSet rs, int rowNum) throws SQLException {
			Long userId = rs.getLong("USER_ID");
			Long resourceId = rs.getLong("RESOURCE_ID");
			Ownership ownership = new NoOwnership();
			switch(ResourceOwnershipEnum.valueOf(rs.getString("PROPIEDAD"))) {
			case INVITE:
				ownership = new Invited(this.fromAction(rs.getString("PRIVILEGIO")));
				break;
			case NO_OWNERSHIP:
				ownership = new NoOwnership();
				break;
			case OWNER:
				ownership = new Owner();
				break;
			};
			Resource resource = resourceRowMapper.mapRow(rs, rowNum);
			resource.setId(resourceId);
			User user = userRowMapper.mapRow(rs, rowNum);
			user.setId(userId);
			ResourceOwnership resourceOwnership = new ResourceOwnership(user, resource, ownership);
			return resourceOwnership;
		}
		
		//TODO: Encontrar una forma mejor
		private Privilege fromAction(String action) {
			switch(action) {
				case "READ":
					return new ReadPrivilge();
				case "WRITE":
					return new WritePrivilege();
				default:
					return new NoPrivilege();
			}
		}

	}

	@Override
	public ResourceOwnership insert(ResourceOwnership resourceOwnership) {
		MapSqlParameterSource paramSource = this.getParamSource(resourceOwnership);
		jdbcTemplate.update(INSERT_OWNERSHIP, paramSource);
		List<Privilege> privileges = resourceOwnership.getPrivilegies();
		privileges.forEach(p -> {
			paramSource.addValue("privilegio", p.getAction().getActionName());
			jdbcTemplate.update(INSERT_PRIVILEGE, paramSource);
		});
		return resourceOwnership;
	}

	@Override
	public ResourceOwnership select(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		List<ResourceOwnership> ownerships = jdbcTemplate.query(SELECT_OWNERSHIP, paramSource, resourceOwnershipRowMapper);
		if(ownerships == null || ownerships.isEmpty())
			return new ResourceOwnership(new User(), new Resource(), new NoOwnership());
		List<Privilege> allPrivileges = new ArrayList<Privilege>();
		for (ResourceOwnership ownership : ownerships) {
			allPrivileges.addAll(ownership.getPrivilegies());
		}
		ResourceOwnership first = ownerships.get(0);
		return new ResourceOwnership(first.getUser(), first.getResource(), first.getOwnership().getWithPrivilege(allPrivileges));
	}

	@Override
	public List<ResourceOwnership> selectByUser(Long userId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		List<ResourceOwnership> ownerships = jdbcTemplate.query(SELECT_OWNERSHIPS_BY_USER, paramSource, resourceOwnershipRowMapper);
		List<ResourceOwnership> returnedOwnerships = new ArrayList<ResourceOwnership>();
		if(ownerships == null || ownerships.isEmpty())
			return new ArrayList<ResourceOwnership>();
		Map<Long, List<ResourceOwnership>> groupByResourceId = ownerships.stream()
			    .collect(Collectors.groupingBy(i -> i.getResource().getId()));
		for (Long resourceId : groupByResourceId.keySet()) {
			List<Privilege> allPrivileges = new ArrayList<Privilege>();
			for (ResourceOwnership ownership : groupByResourceId.get(resourceId)) {
				allPrivileges.addAll(ownership.getPrivilegies());
			}
			ResourceOwnership first = groupByResourceId.get(resourceId).get(0);
			returnedOwnerships.add(new ResourceOwnership(first.getUser(), first.getResource(), first.getOwnership().getWithPrivilege(allPrivileges)));
		}
		return returnedOwnerships;
	}

	@Override
	public List<ResourceOwnership> selectByResource(Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("resource_id", resourceId);
		List<ResourceOwnership> ownerships = jdbcTemplate.query(SELECT_OWNERSHIPS_BY_RESOURCE, paramSource, resourceOwnershipRowMapper);
		if(ownerships == null || ownerships.isEmpty())
			return new ArrayList<ResourceOwnership>();
		return ownerships;
	}

	@Override
	public void update(Long userId, Long resourceId, ResourceOwnership update) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		jdbcTemplate.update(UPDATE_OWNERSHIP, paramSource);
		jdbcTemplate.update(DELETE_OWNERSHIP_PRIVILEGES, paramSource);
		List<Privilege> privileges = update.getPrivilegies();
		privileges.forEach(p -> {
			paramSource.addValue("privilegio", p.getAction().getActionName());
			jdbcTemplate.update(INSERT_PRIVILEGE, paramSource);
		});
	}

	@Override
	public void delete(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		jdbcTemplate.update(DELETE_OWNERSHIP_PRIVILEGES, paramSource);
		jdbcTemplate.update(DELETE_OWNERSHIP, paramSource);
	}

	@Override
	public void deleteByResource(Long resourceId) {
		List<ResourceOwnership> ownerships = this.selectByResource(resourceId);
		for (ResourceOwnership resourceOwnership : ownerships) {
			this.delete(resourceOwnership.getUser().getId(), resourceOwnership.getResource().getId());
		}

	}
	
	private MapSqlParameterSource getParamSource(ResourceOwnership resourceOwnership) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", resourceOwnership.getUser().getId());
		paramSource.addValue("resource_id", resourceOwnership.getResource().getId());
		paramSource.addValue("propiedad", resourceOwnership.getOwnership().getOwnership().name());
		return paramSource;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.resourceOwnershipRowMapper = new ResourceOwnershipRowMapper();
	}

}
