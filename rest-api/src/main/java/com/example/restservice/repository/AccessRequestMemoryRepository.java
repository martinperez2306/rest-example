package com.example.restservice.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.access.AccessRequest;

@Repository	
@ConditionalOnProperty(value="repository.access", havingValue = "mem")
public class AccessRequestMemoryRepository implements AccessRequestRepository, InitializingBean{
	
	private List<AccessRequest> requests;

	@Override
	public AccessRequest insert(AccessRequest request) {
		requests.add(request);
		return request;
	}

	@Override
	public AccessRequest select(Long userId, Long resourceId) {
		return requests.stream().filter(i -> userId.equals(i.getUserId()))
				   .filter(i -> resourceId.equals(i.getResourceId()))
				   .findFirst().orElse(new AccessRequest());
	}

	@Override
	public List<AccessRequest> selectByUser(Long userId) {
		return requests.stream().filter(i -> userId.equals(i.getUserId())).collect(Collectors.toList());
	}
	
	@Override
	public List<AccessRequest> selectByResource(Long resourceId) {
		return requests.stream().filter(i -> resourceId.equals(i.getResourceId())).collect(Collectors.toList());
	}

	@Override
	public void delete(Long userId, Long resourceId) {
		requests = requests.stream().filter(i -> !userId.equals(i.getUserId()) && !resourceId.equals(i.getResourceId()) ).collect(Collectors.toList());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		requests = new ArrayList<AccessRequest>();
	}

}
