package com.example.restservice.repository;

public interface ResourceFileRepository {
	
	public void insert(Long resourceId, byte[] bytes);
	
	public byte[] select(Long resourceId);
	
	public void update(Long resourceId, byte[] bytes);
	
	public void delete (Long resourceId);
	
	public Boolean containsFile(Long resourceId);

}
