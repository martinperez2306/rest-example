package com.example.restservice.repository;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;
import com.example.restservice.rowmapper.ResourceRowMapper;

@Repository	
@ConditionalOnProperty(value="repository.resource", havingValue = "dao")
public class ResourceDao implements ResourceRepository, InitializingBean {
	
	private final static String RESOURCE_TABLE = "recursos";
	
	private final static String SELECT_RESOURCES = "SELECT ID, NOMBRE, DETALLE, PRIVACIDAD FROM "+ RESOURCE_TABLE;
	private final static String SELECT_RESOURCE = SELECT_RESOURCES + " WHERE ID = :id";
	private final static String INSERT_RESOURCE = "INSERT INTO "+ RESOURCE_TABLE +" (NOMBRE, DETALLE, PRIVACIDAD) VALUES (:nombre, :detalle, :privacidad)";
	private final static String UPDATE_RESOURCE = "UPDATE "+ RESOURCE_TABLE +" SET NOMBRE = :nombre, DETALLE = :detalle, PRIVACIDAD = :privacidad WHERE ID = :id";
	private final static String DELETE_RESOURCE = "DELETE FROM "+ RESOURCE_TABLE +" WHERE ID = :id";
	
	private final static String SELECT_RESOURCE_BY_PRIVACY = SELECT_RESOURCES + " WHERE PRIVACIDAD = :privacidad";
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private ResourceRowMapper resourceRowMapper;
	
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<Resource> select() {
		return jdbcTemplate.query(SELECT_RESOURCES, resourceRowMapper);
	}

	@Override
	public Resource select(Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", resourceId);
		List<Resource> resources = jdbcTemplate.query(SELECT_RESOURCE, paramSource, resourceRowMapper);
		if(resources == null || resources.isEmpty())
			return new Resource();
		return resources.get(0);
	}

	@Override
	public Resource insert(Resource resource) {
		MapSqlParameterSource paramSource = this.getParamSource(resource);
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(INSERT_RESOURCE, paramSource, holder);
		resource.setId(holder.getKey().longValue());
		return resource;
	}

	@Override
	public Resource update(Long resourceId, Resource resource) {
		MapSqlParameterSource paramSource = this.getParamSource(resource);
		paramSource.addValue("id", resourceId);
		jdbcTemplate.update(UPDATE_RESOURCE, paramSource);
		return resource;
	}

	@Override
	public void delete(Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", resourceId);
		jdbcTemplate.update(DELETE_RESOURCE, paramSource);
	}

	@Override
	public List<Resource> selectByPrivacy(ResourcePrivacy privacy) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("privacidad", privacy.name());
		List<Resource> resources = jdbcTemplate.query(SELECT_RESOURCE_BY_PRIVACY, paramSource, resourceRowMapper);
		if(resources == null) {
			resources = new ArrayList<Resource>();
		}
		return resources;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	private MapSqlParameterSource getParamSource(Resource resource) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("nombre", resource.getName());
		paramSource.addValue("detalle", resource.getDetail());
		if(resource.isVisible()) {
			paramSource.addValue("privacidad", ResourcePrivacy.PUBLIC.name());
		}else {
			paramSource.addValue("privacidad", ResourcePrivacy.PRIVATE.name());
		}
		return paramSource;
	}

}
