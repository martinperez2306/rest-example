package com.example.restservice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.privileges.NoPrivilege;
import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ReadPrivilge;
import com.example.restservice.model.privileges.WritePrivilege;

@Repository	
@ConditionalOnProperty(value="repository.invitation", havingValue = "dao")
public class InvitationDao implements InvitationRepository, InitializingBean{
	
	private final static String INVITATION_TABLE = "invitaciones";
	private final static String PRIVILEGE_TABLE = "privilegios";
	
	private final static String SELECT_INVITATIONS = "SELECT * FROM "+ INVITATION_TABLE + " AS I, " + PRIVILEGE_TABLE + " AS P WHERE I.USER_ID = P.USER_ID AND I.RESOURCE_ID = P.RESOURCE_ID";
	private final static String SELECT_INVITATION = SELECT_INVITATIONS + " AND I.USER_ID = :user_id AND I.RESOURCE_ID = :resource_id";
	private final static String INSERT_INVITATION = "INSERT INTO "+ INVITATION_TABLE +" (USER_ID, RESOURCE_ID, FECHA_LIMITE) VALUES (:user_id, :resource_id, :fecha_limite)";
	private final static String DELETE_INVITATION = "DELETE FROM "+ INVITATION_TABLE +" WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	
	private final static String SELECT_INVITATION_BY_USER = SELECT_INVITATIONS + " AND I.USER_ID = :user_id";
	
	private final static String INSERT_PRIVILEGE = "INSERT INTO "+ PRIVILEGE_TABLE +" (USER_ID, RESOURCE_ID, PRIVILEGIO) VALUES (:user_id, :resource_id, :privilegio)";
	private final static String DELETE_INVITATION_PRIVILEGES = "DELETE FROM "+ PRIVILEGE_TABLE +" WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	private final static String SELECT_INVITATION_EXPIRED = "SELECT * FROM "+ INVITATION_TABLE +" WHERE fecha_limite < (select current_timestamp())";

	@Autowired
	private DataSource dataSource;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private JdbcTemplate nonamed;
	
	private InvitationRowMapper invitationRowMapper;
	
	private class InvitationRowMapper implements RowMapper<Invitation> {
		@Override
		public Invitation mapRow(ResultSet rs, int rowNum) throws SQLException {
			Long userId = rs.getLong("USER_ID");
			Long resourceId = rs.getLong("RESOURCE_ID");
			Date limitDate = rs.getDate("FECHA_LIMITE");
			Privilege privilege = this.fromAction(rs.getString("PRIVILEGIO"));
			List<Privilege> privileges = new ArrayList<Privilege>();
			privileges.add(privilege);
			Invitation invitation = new Invitation(userId, resourceId, limitDate, privileges);
			return invitation;
		}
		
		//TODO: Encontrar una forma mejor
		private Privilege fromAction(String action) {
			switch(action) {
				case "READ":
					return new ReadPrivilge();
				case "WRITE":
					return new WritePrivilege();
				default:
					return new NoPrivilege();
			}
		}

	}
	
	@Override
	public Invitation insert(Invitation invitation) {
		MapSqlParameterSource paramSource = this.getParamSource(invitation);
		jdbcTemplate.update(INSERT_INVITATION, paramSource);
		List<Privilege> privileges = invitation.getPrivileges();
		privileges.forEach(p -> {
			paramSource.addValue("privilegio", p.getAction().getActionName());
			jdbcTemplate.update(INSERT_PRIVILEGE, paramSource);
		});
		return invitation;
	}

	@Override
	public Invitation select(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		List<Invitation> invitations = jdbcTemplate.query(SELECT_INVITATION, paramSource, invitationRowMapper);
		if(invitations == null || invitations.isEmpty())
			return new Invitation(0L, 0L, new Date());
		List<Privilege> allPrivileges = new ArrayList<Privilege>();
		for (Invitation invitation : invitations) {
			allPrivileges.addAll(invitation.getPrivileges());
		}
		return new Invitation(userId, resourceId, invitations.get(0).getLimitDate(),allPrivileges);
	}

	@Override
	public List<Invitation> selectByUser(Long userId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		List<Invitation> invitations = jdbcTemplate.query(SELECT_INVITATION_BY_USER, paramSource, invitationRowMapper);
		List<Invitation> returnedIntivations = new ArrayList<Invitation>();
		if(invitations == null || invitations.isEmpty())
			return new ArrayList<Invitation>();
		Map<Long, List<Invitation>> groupByResourceId = invitations.stream()
			    .collect(Collectors.groupingBy(i -> i.getResourceId()));
		for (Long resourceId : groupByResourceId.keySet()) {
			List<Privilege> allPrivileges = new ArrayList<Privilege>();
			for (Invitation invitation : groupByResourceId.get(resourceId)) {
				allPrivileges.addAll(invitation.getPrivileges());
			}
			returnedIntivations.add(new Invitation(userId, resourceId, invitations.get(0).getLimitDate(),allPrivileges));
		}
		return returnedIntivations;
	}

	@Override
	public void delete(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		jdbcTemplate.update(DELETE_INVITATION_PRIVILEGES, paramSource);
		jdbcTemplate.update(DELETE_INVITATION, paramSource);
	}

	@Override
	public void deleteExpiredInvitations() {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("fecha_actual", new java.sql.Date(System.currentTimeMillis()), java.sql.Types.TIMESTAMP);
		List<Invitation> expireds = nonamed.query(SELECT_INVITATION_EXPIRED, new RowMapper<Invitation>() {
			@Override
			public Invitation mapRow(ResultSet rs, int rowNum) throws SQLException {
				Long userId = rs.getLong("USER_ID");
				Long resourceId = rs.getLong("RESOURCE_ID");
				Date limitDate = rs.getDate("FECHA_LIMITE");
				Invitation invitation = new Invitation(userId, resourceId, limitDate);
				return invitation;
			}
		});
		expireds.forEach(e -> this.delete(e.getUserId(), e.getResourceId()));
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.invitationRowMapper = new InvitationRowMapper();
		this.nonamed = new JdbcTemplate(dataSource);
	}
	
	private MapSqlParameterSource getParamSource(Invitation invitation) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", invitation.getUserId());
		paramSource.addValue("resource_id", invitation.getResourceId());
		paramSource.addValue("fecha_limite", invitation.getLimitDate());
		return paramSource;
	}

}
