package com.example.restservice.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.invitation.Invitation;

@Repository
@ConditionalOnProperty(value="repository.invitation", havingValue = "mem")
public class InvitationMemoryRepository implements InvitationRepository, InitializingBean{
	
	private List<Invitation> invitations;
	private Long sequence;

	@Override
	public Invitation insert(Invitation invitation) {
		sequence++;
		invitations.add(invitation);
		return invitation;
	}

	@Override
	public Invitation select(Long userId, Long resourceId) {
		return invitations.stream().filter(i -> userId.equals(i.getUserId()))
								   .filter(i -> resourceId.equals(i.getResourceId()))
								   .findFirst().orElse(new Invitation(0L,0L,new Date(System.currentTimeMillis())));
	}

	@Override
	public List<Invitation> selectByUser(Long userId) {
		return invitations.stream().filter(i -> userId.equals(i.getUserId())).collect(Collectors.toList());
	}

	@Override
	public void delete(Long userId, Long resourceId) {
		invitations = invitations.stream().filter(i -> !userId.equals(i.getUserId()) && !resourceId.equals(i.getResourceId()) ).collect(Collectors.toList());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.invitations = new ArrayList<Invitation>();
		this.sequence = 0L;
	}

	@Override
	public void deleteExpiredInvitations() {
		invitations = invitations.stream().filter(i -> !i.isExpired() ).collect(Collectors.toList());
	}

}
