package com.example.restservice.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;

@Repository
@ConditionalOnProperty(value="repository.resource", havingValue = "mem")
public class ResourceMemoryRepository implements ResourceRepository,  InitializingBean{
	
	private Map<Long, Resource> resources;
	private Long sequence;

	@Override
	public List<Resource> select() {
		return resources.values().stream().collect(Collectors.toList());
	}

	@Override
	public Resource select(Long resourceId) {
		return resources.values().stream().filter(r -> r.getId().equals(resourceId)).findFirst().orElse(new Resource());
	}

	@Override
	public Resource insert(Resource resource) {
		sequence++;
		resource.setId(sequence);
		resources.put(sequence, resource);
		return resource;
	}

	@Override
	public Resource update(Long resourceId, Resource resource) {
		Resource oldResource = resources.remove(resourceId);
		resource.setId(resourceId);
		if(oldResource != null)
			resources.put(resourceId, resource);
		return resource;
	}

	@Override
	public void delete(Long resourceId) {
		resources.remove(resourceId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.resources = new HashMap<Long, Resource>();
		this.sequence = 0L;
	}

	@Override
	public List<Resource> selectByPrivacy(ResourcePrivacy privacy) {
		List<Resource> resources = new ArrayList<Resource>();
		switch(privacy) {
			case PUBLIC:
				resources = this.selectPublicResources();
				break;
			case PRIVATE:
				resources = this.selectPrivateResources();
				break;
		}
		return resources;
	}
	
	private List<Resource> selectPublicResources(){
		return resources.values().stream().filter(r -> r.isVisible()).collect(Collectors.toList());
	}
	
	private List<Resource> selectPrivateResources(){
		return resources.values().stream().filter(r -> !r.isVisible()).collect(Collectors.toList());
	}

}
