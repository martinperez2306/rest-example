package com.example.restservice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository	
@ConditionalOnProperty(value="repository.resource.file", havingValue = "dao")
public class ResourceFileDao implements ResourceFileRepository, InitializingBean{
	
	private final static String RESOURCE_TABLE = "recursos";
	
	private final static String SELECT_FILES = "SELECT ARCHIVO FROM "+ RESOURCE_TABLE;
	private final static String SELECT_FILE = SELECT_FILES + " WHERE ID = :id";
	private final static String INSERT_FILE = "UPDATE "+ RESOURCE_TABLE +" SET ARCHIVO = :archivo WHERE ID = :id";
	private final static String COUNT_FILES = "SELECT COUNT(1) FROM"  + RESOURCE_TABLE + " WHERE ID = :id";
	
	@Autowired
	private DataSource dataSource;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private FileRowMapper fileRowMapper;
	
	private class FileRowMapper implements RowMapper<byte[]> {
		@Override
		public byte[] mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getBytes("ARCHIVO");
		}
	}

	@Override
	public void insert(Long resourceId, byte[] bytes) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", resourceId);
		paramSource.addValue("archivo", bytes);
		jdbcTemplate.update(INSERT_FILE, paramSource);
	}

	@Override
	public byte[] select(Long resourceId) {
		byte[] bytes = null;
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", resourceId);
		List<byte[]> files = jdbcTemplate.query(SELECT_FILE, paramSource, fileRowMapper);
		if(files != null && !files.isEmpty())
			bytes = files.get(0);
		return bytes;
	}

	@Override
	public void update(Long resourceId, byte[] bytes) {
		this.insert(resourceId, bytes);
	}

	@Override
	public void delete(Long resourceId) {
		
	}

	@Override
	public Boolean containsFile(Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", resourceId);
		Integer count = this.jdbcTemplate.queryForObject(COUNT_FILES, paramSource, Integer.class);
		return count > 0;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.fileRowMapper = new FileRowMapper();
	}

}
