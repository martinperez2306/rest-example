package com.example.restservice.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(value="repository.resource.file", havingValue = "mem")
public class ResourceFileMemoryRepository implements ResourceFileRepository, InitializingBean{
	
	private Map<Long, File> files;

	@Override
	public void insert(Long resourceId, byte[] bytes) {
		try {
			File tempFile = File.createTempFile("File - "+resourceId.toString(),null);
			OutputStream outStream = new FileOutputStream(tempFile);
		    outStream.write(bytes);
			files.put(resourceId, tempFile);
			outStream.close();
		} catch (IOException e) {
			
		}
	}

	@Override
	public byte[] select(Long resourceId) {
		byte[] bytesArray = null;
		FileInputStream fileInputStream = null;
		try {
			File file = files.get(resourceId);
			bytesArray = new byte[(int) file.length()];
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bytesArray);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
		}
		return bytesArray;
	}

	@Override
	public void update(Long resourceId, byte[] updated) {
		this.delete(resourceId);
		this.insert(resourceId, updated);
	}

	@Override
	public void delete(Long resourceId) {
		File file = files.get(resourceId);
		if(file != null) {
			file.deleteOnExit();
			files.remove(resourceId);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		files = new HashMap<Long, File>();
	}

	@Override
	public Boolean containsFile(Long resourceId) {
		return files.containsKey(resourceId);
	}
	
}
