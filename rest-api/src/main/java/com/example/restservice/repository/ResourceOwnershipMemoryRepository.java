package com.example.restservice.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.privileges.NoOwnership;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;

@Repository
@ConditionalOnProperty(value="repository.ownership", havingValue = "mem")
public class ResourceOwnershipMemoryRepository implements ResourceOwnershipRepository, InitializingBean{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ResourceRepository resourceRepository;
	
	private Map<Long, ResourceOwnership> ownerships;
	private Long sequence;
	
	@Override
	public ResourceOwnership insert(ResourceOwnership resourceOwnership) {
		sequence++;
		resourceOwnership.setId(sequence);
		ownerships.put(sequence, resourceOwnership);
		return resourceOwnership;
	}

	//TODO: Puedo usar lambda como en selectByUser
	@Override
	public ResourceOwnership select(Long userId, Long resourceId) {
		Long id = getOwnershipId(userId, resourceId);
		ResourceOwnership resourceOwnership = ownerships.get(id);
		if(resourceOwnership == null)
			resourceOwnership = new ResourceOwnership(null, null, new NoOwnership());
		User user = userRepository.select(userId);
		Resource resource = resourceRepository.select(resourceId);
		return new ResourceOwnership(user, resource, resourceOwnership.getOwnership());
	}

	@Override
	public List<ResourceOwnership> selectByUser(Long userId) {
		return ownerships.values().stream()
								  .filter(ro -> userId.equals(ro.getUser().getId()))
								  .map(ro -> new ResourceOwnership(userRepository.select(userId), resourceRepository.select(ro.getResource().getId()), ro.getOwnership()))
								  .collect(Collectors.toList());
	}
	
	@Override
	public List<ResourceOwnership> selectByResource(Long resourceId) {
		return ownerships.values().stream()
				  .filter(ro -> resourceId.equals(ro.getResource().getId()))
				  .map(ro -> new ResourceOwnership(userRepository.select(ro.getUser().getId()), resourceRepository.select(resourceId), ro.getOwnership()))
				  .collect(Collectors.toList());
	}

	@Override
	public void update(Long userId, Long resourceId, ResourceOwnership update) {
		Long id = getOwnershipId(userId, resourceId);
		ResourceOwnership resourceOwnership = ownerships.get(id);
		if(resourceOwnership != null) {
			ownerships.remove(id);
			ownerships.put(id, update);
		}
	}
	
	@Override
	public void delete(Long userId, Long resourceId) {
		Long idOwnership = getOwnershipId(userId, resourceId);
		ownerships.remove(idOwnership);
	}
	
	@Override
	public void deleteByResource(Long resourceId) {
		List<Long> ids = ownerships.values().stream().filter(ro -> resourceId.equals(ro.getResource().getId())).map(ro -> ro.getId()).collect(Collectors.toList());
		ids.forEach(id -> this.ownerships.remove(id));
	}

	private Long getOwnershipId(Long userId, Long resourceId) {
		return ownerships.values()
				.stream()
				.filter(ro -> userId.equals(ro.getUser().getId()) && resourceId.equals(ro.getResource().getId()))
				.map(ro -> ro.getId())
				.findFirst()
				.orElse(0L);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		ownerships = new HashMap<Long, ResourceOwnership>();
		sequence = 0L;
	}

}
