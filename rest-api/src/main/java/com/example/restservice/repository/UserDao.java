package com.example.restservice.repository;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.user.User;
import com.example.restservice.rowmapper.UserRowMapper;

@Repository	
@ConditionalOnProperty(value="repository.user", havingValue = "dao")
public class UserDao implements UserRepository, InitializingBean{
	
	private final static String USER_TABLE = "usuarios";
	
	private final static String SELECT_USERS = "SELECT * FROM "+ USER_TABLE;
	private final static String SELECT_USER = SELECT_USERS + " WHERE ID = :id";
	private final static String INSERT_USER = "INSERT INTO "+ USER_TABLE +" (USERNAME, PASSWORD, NOMBRE, APELLIDO, AVATAR, EMAIL, NACIMIENTO) VALUES (:username, :password, :nombre, :apellido, :avatar, :email, :nacimiento)";
	private final static String UPDATE_USER = "UPDATE "+ USER_TABLE +" SET USERNAME = :username, PASSWORD = :password, NOMBRE = :nombre, APELLIDO = :apellido, AVATAR = :avatar, EMAIL = :email, NACIMIENTO = :nacimiento WHERE ID = :id";
	private final static String DELETE_USER = "DELETE FROM "+ USER_TABLE +" WHERE ID = :id";
	
	private final static String SELECT_USER_BY_USERNAME = SELECT_USERS + " WHERE USERNAME = :username";
	
	@Autowired
	private DataSource dataSource;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	private UserRowMapper userRowMapper;
	
	@Override
	public List<User> select() {
		return jdbcTemplate.query(SELECT_USERS, userRowMapper);
	}

	@Override
	public User select(Long userId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", userId);
		List<User> users = jdbcTemplate.query(SELECT_USER, paramSource, userRowMapper);
		if(users == null || users.isEmpty())
			return new User();
		return users.get(0);
	}

	@Override
	public User selectByUsername(String username) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("username", username);
		List<User> users = jdbcTemplate.query(SELECT_USER_BY_USERNAME, paramSource, userRowMapper);
		if(users == null ||  users.isEmpty()) {
			return new User();
		}
		return users.get(0);
	}

	@Override
	public User insert(User user) {
		MapSqlParameterSource paramSource = this.getParamSource(user);
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(INSERT_USER, paramSource, holder);
		user.setId(holder.getKey().longValue());
		return user;
	}

	@Override
	public User update(Long userId, User user) {
		MapSqlParameterSource paramSource = this.getParamSource(user);
		paramSource.addValue("id", userId);
		jdbcTemplate.update(UPDATE_USER, paramSource);
		return user;
	}

	@Override
	public void delete(Long userId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", userId);
		jdbcTemplate.update(DELETE_USER, paramSource);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	private MapSqlParameterSource getParamSource(User user) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("nombre", user.getFirstname());
		paramSource.addValue("apellido", user.getSurname());
		paramSource.addValue("username", user.getUsername());
		paramSource.addValue("password", user.getPassword());
		paramSource.addValue("avatar", user.getAvatar());
		paramSource.addValue("email", user.getEmail());
		paramSource.addValue("nacimiento", user.getBirthDate());
		return paramSource;
	}

}
