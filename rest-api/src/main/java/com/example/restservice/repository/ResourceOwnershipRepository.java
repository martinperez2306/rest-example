package com.example.restservice.repository;

import java.util.List;

import com.example.restservice.model.privileges.ResourceOwnership;

public interface ResourceOwnershipRepository {
	
	public ResourceOwnership insert(ResourceOwnership resourceOwnership);
	
	public ResourceOwnership select(Long userId, Long resourceId);
	
	public List<ResourceOwnership> selectByUser(Long userId);
	
	public List<ResourceOwnership> selectByResource(Long resourceId);
	
	public void update(Long userId, Long resourceId, ResourceOwnership update);

	public void delete(Long userId, Long resourceId);
	
	public void deleteByResource(Long resourceId);
	
}
