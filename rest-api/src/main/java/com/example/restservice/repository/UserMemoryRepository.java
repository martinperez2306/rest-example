package com.example.restservice.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.user.User;

@Repository
@ConditionalOnProperty(value="repository.user", havingValue = "mem")
public class UserMemoryRepository implements UserRepository, InitializingBean{
	
	private Map<Long, User> users;
	private Long sequence;

	@Override
	public List<User> select() {
		return users.values().stream().collect(Collectors.toList());
	}

	@Override
	public User select(Long userId) {
		return users.values().stream().filter(u -> u.getId().equals(userId)).findFirst().orElse(new User());
	}
	
	@Override
	public User selectByUsername(String username) {
		return users.values().stream().filter(u -> u.getUsername().equals(username)).findFirst().orElse(new User());
	}

	@Override
	public User insert(User user) {
		sequence++;
		user.setId(sequence);
		users.put(user.getId(), user);
		return user;
	}

	@Override
	public User update(Long userId, User user) {
		User oldUser = users.remove(userId);
		user.setId(userId);
		if(oldUser != null)
			users.put(userId, user);
		return user;
	}

	@Override
	public void delete(Long userId) {
		users.remove(userId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.users = new HashMap<Long, User>();
		this.sequence = 0L;
	}
	
}
