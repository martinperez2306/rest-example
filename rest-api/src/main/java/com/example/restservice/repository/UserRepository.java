package com.example.restservice.repository;

import java.util.List;

import com.example.restservice.model.user.User;

public interface UserRepository {
	
	public List<User> select();
	
	public User select(Long userId);
	
	public User selectByUsername(String username);
	
	public User insert(User user);
	
	public User update(Long userId, User user);
	
	public void delete(Long userId);

}
