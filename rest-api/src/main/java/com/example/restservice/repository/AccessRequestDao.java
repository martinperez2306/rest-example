package com.example.restservice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.restservice.model.access.AccessRequest;

@Repository	
@ConditionalOnProperty(value="repository.access", havingValue = "dao")
public class AccessRequestDao implements AccessRequestRepository, InitializingBean{
	
private final static String ACCESS_REQUEST_TABLE = "solicitudes";
	
	private final static String SELECT_ACCESS_REQUESTS = "SELECT * FROM "+ ACCESS_REQUEST_TABLE;
	private final static String SELECT_ACCESS_REQUEST = SELECT_ACCESS_REQUESTS + " WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	private final static String INSERT_ACCESS_REQUEST = "INSERT INTO "+ ACCESS_REQUEST_TABLE +" (USER_ID, RESOURCE_ID) VALUES (:user_id, :resource_id)";
	private final static String DELETE_ACCESS_REQUEST = "DELETE FROM "+ ACCESS_REQUEST_TABLE +" WHERE USER_ID = :user_id AND RESOURCE_ID = :resource_id";
	
	private final static String SELECT_BY_USER = SELECT_ACCESS_REQUESTS + " WHERE USER_ID = :user_id";
	private final static String SELECT_BY_RESOURCE = SELECT_ACCESS_REQUESTS + " WHERE RESOURCE_ID = :resource_id";
	
	@Autowired
	private DataSource dataSource;
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private AccessRequestRowMapper accessRequestRowMapper;
	
	private class AccessRequestRowMapper implements RowMapper<AccessRequest> {
		@Override
		public AccessRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
			AccessRequest accessRequest = new AccessRequest();
			accessRequest.setUserId(rs.getLong("USER_ID"));
			accessRequest.setResourceId(rs.getLong("RESOURCE_ID"));
			return accessRequest;
		}
	}

	@Override
	public AccessRequest insert(AccessRequest request) {
		MapSqlParameterSource paramSource = this.getParamSource(request);
		jdbcTemplate.update(INSERT_ACCESS_REQUEST, paramSource);
		return request;
	}

	@Override
	public AccessRequest select(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		List<AccessRequest> accessRequests = jdbcTemplate.query(SELECT_ACCESS_REQUEST, paramSource, accessRequestRowMapper);
		if(accessRequests == null || accessRequests.isEmpty())
			return new AccessRequest();
		return accessRequests.get(0);
	}

	@Override
	public List<AccessRequest> selectByUser(Long userId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		List<AccessRequest> accessRequests = jdbcTemplate.query(SELECT_BY_USER, paramSource, accessRequestRowMapper);
		if(accessRequests == null || accessRequests.isEmpty())
			return new ArrayList<AccessRequest>();
		return accessRequests;
	}

	@Override
	public List<AccessRequest> selectByResource(Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("resource_id", resourceId);
		List<AccessRequest> accessRequests = jdbcTemplate.query(SELECT_BY_RESOURCE, paramSource, accessRequestRowMapper);
		if(accessRequests == null || accessRequests.isEmpty())
			return new ArrayList<AccessRequest>();
		return accessRequests;
	}

	@Override
	public void delete(Long userId, Long resourceId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", userId);
		paramSource.addValue("resource_id", resourceId);
		jdbcTemplate.update(DELETE_ACCESS_REQUEST, paramSource);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.accessRequestRowMapper = new AccessRequestRowMapper();
	}
	
	private MapSqlParameterSource getParamSource(AccessRequest accessRequest) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("user_id", accessRequest.getUserId());
		paramSource.addValue("resource_id", accessRequest.getResourceId());
		return paramSource;
	}

}
