package com.example.restservice.repository;

import java.util.List;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;

public interface ResourceRepository {
	
public List<Resource> select();
	
	public Resource select(Long resourceId);
	
	public Resource insert(Resource resource);
	
	public Resource update(Long resourceId, Resource resource);
	
	public void delete(Long resourceId);

	public List<Resource> selectByPrivacy(ResourcePrivacy privacy);

}
