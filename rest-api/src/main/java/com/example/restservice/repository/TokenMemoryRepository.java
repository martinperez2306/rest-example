package com.example.restservice.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(value="repository.token", havingValue = "mem")
public class TokenMemoryRepository implements TokenRepository, InitializingBean{
	
	private Map<String, Long> userTokens;

	@Override
	public void insertTokenForUser(String token, Long userId) {
		userTokens.put(token, userId);
	}

	@Override
	public Long selectUserWithToken(String token) {
		return userTokens.get(token);
	}

	@Override
	public void deleteTokenUser(String token) {
		userTokens.remove(token);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		userTokens = new HashMap<String, Long>();
	}

}
