package com.example.restservice.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.restservice.model.invitation.InvitationRestInput;
import com.example.restservice.model.resource.ResourceRestInput;
import com.example.restservice.model.response.AccessRequestCollectionResponse;
import com.example.restservice.model.response.AccessRequestResponse;
import com.example.restservice.model.response.InvitationCollectionResponse;
import com.example.restservice.model.response.ResourceCollectionResponse;
import com.example.restservice.model.response.ResourceResponse;

public interface ResourceApi {

	@GetMapping(value="/resources", produces = { "application/json" })
	public ResponseEntity<ResourceCollectionResponse> getPublicResources();

	@GetMapping(value="/resources/{id}", produces = { "application/json" })
	public ResponseEntity<ResourceResponse> getResource(@RequestHeader("Authorization") String token, @PathVariable("id") Long resourceId);

	@PostMapping(value="/resources", produces = { "application/json" },consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE,  MediaType.APPLICATION_OCTET_STREAM_VALUE})
	public ResponseEntity<ResourceResponse> addResource(@RequestHeader("Authorization") String token, @RequestParam(value="resource") ResourceRestInput resource, @RequestParam(value="file") MultipartFile file);

	@PostMapping(value="/resources/{id}", produces = { "application/json" },consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE,  MediaType.APPLICATION_OCTET_STREAM_VALUE})
	public ResponseEntity<ResourceResponse> updateResource(@RequestHeader("Authorization") String token, @PathVariable("id") Long resourceId, @RequestParam(value="resource") ResourceRestInput resource, @RequestParam(value="file") MultipartFile file);

	@DeleteMapping(value="/resources/{id}", produces = { "application/json" })
	public ResponseEntity<ResourceResponse> deleteResource(@RequestHeader("Authorization") String token, @PathVariable("id")Long resourceId);
	
	@GetMapping(value="/resources/{id}/download", produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	public ResponseEntity<byte[]> downloadResourceFile(@RequestHeader("Authorization") String token, @PathVariable("id") Long resourceId);

	@GetMapping(value="/users/resources", produces = { "application/json" })
	public ResponseEntity<ResourceCollectionResponse> getUserResources(@RequestHeader("Authorization") String token);

	@GetMapping(value="/resources/access", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<AccessRequestCollectionResponse> getAccessRequestsToResoruce(@RequestHeader("Authorization") String token);

	@PostMapping(value="/resources/{resourceId}/access", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<AccessRequestResponse> requestAccessToResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId);

	@PostMapping(value="/resources/{resourceId}/access/{userId}", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<AccessRequestResponse> acceptAccessToResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId, @PathVariable("userId") Long userId);

	@DeleteMapping(value="/resources/{resourceId}/access/{userId}", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<AccessRequestResponse> rejectAccessToResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId, @PathVariable("userId") Long userId);

	@GetMapping(value="/resources/invitations", produces = { "application/json" })
	public ResponseEntity<InvitationCollectionResponse> getInvitations(@RequestHeader("Authorization") String token);

	@PostMapping(value="/resources/{resourceId}/invitations/{userId}", produces = { "application/json" })
	public ResponseEntity<ResourceResponse> inviteUserToResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId, @PathVariable("userId") Long userId, @RequestBody InvitationRestInput invitation);

	@PostMapping(value="/resources/{resourceId}/share/{userId}", produces = { "application/json" })
	public ResponseEntity<ResourceResponse> shareResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId, @PathVariable("userId") Long userId);

	@DeleteMapping(value="/resources/{resourceId}/share/{userId}", produces = { "application/json" })
	public ResponseEntity<ResourceResponse> forbidResource(@RequestHeader("Authorization") String token, @PathVariable("resourceId") Long resourceId, @PathVariable("userId") Long userId);
}
