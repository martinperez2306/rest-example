package com.example.restservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.restservice.auth.UserCredentials;
import com.example.restservice.model.response.LoginResponse;

public interface LoginApi {
	
	@PostMapping(value="/login", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<LoginResponse> login(@RequestBody UserCredentials credentials);

}
