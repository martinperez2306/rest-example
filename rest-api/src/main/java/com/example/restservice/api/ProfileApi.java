package com.example.restservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.restservice.model.response.UserResponse;
import com.example.restservice.model.user.UserRestInput;

public interface ProfileApi {
	
	@GetMapping(value="/users/profile", produces = { "application/json" })
	public ResponseEntity<UserResponse> getProfile(@RequestHeader("Authorization") String token);
	
	@PutMapping(value="/users/profile", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<UserResponse> updateProfile(@RequestHeader("Authorization") String token, @RequestBody UserRestInput userRestInput);
	
}
