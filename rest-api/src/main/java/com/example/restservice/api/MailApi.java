package com.example.restservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.restservice.model.response.UserResponse;

public interface MailApi {
	
	@GetMapping(value="/mails/users/{userId}/confirm", produces = { "application/json" })
	public ResponseEntity<UserResponse> confirmUser(@PathVariable("userId") Long userId);

}
