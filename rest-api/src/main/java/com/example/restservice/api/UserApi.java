package com.example.restservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.example.restservice.model.response.UserCollectionResponse;
import com.example.restservice.model.response.UserResponse;
import com.example.restservice.model.user.UserRegister;

public interface UserApi {
	
	@GetMapping(value="/users", produces = { "application/json" })
	public ResponseEntity<UserCollectionResponse> getUsers();
	
	//TODO: Debe devolver el usuario creado?
	//TODO: Como se construye el header location?
	@PostMapping(value="/users", produces = { "application/json" },consumes = { "application/json" })
	public ResponseEntity<UserResponse> addUser(@RequestBody UserRegister user);
	
	@GetMapping(value="/users/{id}", produces = { "application/json" })
	public ResponseEntity<UserResponse> getUser(@PathVariable("id") Long userId);
	
	//TODO: Debe devolver el usuario borrado?
	@DeleteMapping(value="/users/{id}", produces = { "application/json" })
	public ResponseEntity<UserResponse> deleteUser(@PathVariable("id")Long userId);
	
	@GetMapping(value="/resources/{id}/users", produces = { "application/json" })
	public ResponseEntity<UserCollectionResponse> getUserOfResource(@RequestHeader("Authorization") String token, @PathVariable("id")Long resourceId);

}
