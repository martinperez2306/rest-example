package com.example.restservice.adapter;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRegister;

@Component	
public class RegisterAdapter implements InitializingBean{
	
	private UserRegisterToUserAdapter restToModel;
	
	public User adaptToModel(UserRegister userRegister) {
		return restToModel.adaptToModel(userRegister);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		restToModel = new UserRegisterToUserAdapter();
	}
}
