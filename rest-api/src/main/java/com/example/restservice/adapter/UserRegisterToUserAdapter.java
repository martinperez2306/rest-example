package com.example.restservice.adapter;

import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRegister;
import com.example.restservice.model.user.UserRestInput;

public class UserRegisterToUserAdapter {

	public User adaptToModel(UserRegister userRegister) {
		User user = new User();
		UserRestInput userData = userRegister.getUserData();
		if(userData != null) {
			user.setAvatar(userData.getAvatar());
			user.setBirthDate(userData.getBirthDate());
			user.setEmail(userData.getEmail());
			user.setFirstname(userData.getFirstname());
			user.setSurname(userData.getSurname());
		}
		user.setUsername(userRegister.getUsername());
		user.setPassword(userRegister.getPassword());
		return user;
	}

}
