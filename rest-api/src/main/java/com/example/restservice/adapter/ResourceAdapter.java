package com.example.restservice.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;
import com.example.restservice.model.resource.ResourceRestData;
import com.example.restservice.model.resource.ResourceRestInput;

@Component
public class ResourceAdapter {
	
	public Resource adaptToModel(ResourceRestInput request) {
		Resource resource = new Resource();
		resource.setName(request.getName());
		resource.setDetail(request.getDetail());
		if(ResourcePrivacy.PRIVATE.equals(request.getPrivacy()))
			resource.setAsPrivate();
		return resource;
	}
	
	public ResourceRestData adaptToRest(Resource resource) {
		ResourceRestData restData = new ResourceRestData();
		restData.setId(resource.getId());
		restData.setName(resource.getName());
		restData.setDetail(resource.getDetail());
		if(resource.isVisible())
			restData.setPrivacy(ResourcePrivacy.PUBLIC);
		else
			restData.setPrivacy(ResourcePrivacy.PRIVATE);
		return restData;
	}

	public List<ResourceRestData> adaptToRest(List<Resource> resources) {
		List<ResourceRestData> restData = new ArrayList<ResourceRestData>();
		for (Resource resource : resources) {
			restData.add(this.adaptToRest(resource));
		}
		return restData;
	}
	

}
