package com.example.restservice.adapter;


import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRestData;
import com.example.restservice.model.user.UserRestInput;

@Component
public class UserAdapter implements InitializingBean{

	private UserRestToUserAdapter restToModel;
	private UserToUserRestAdapter modelToRest;
	
	public User adaptToModel(UserRestInput request) {
		return restToModel.adapt(request);
	}
	
	public UserRestData adaptToRest(User user) {
		return this.adaptToRest(user, false);
	}
	
	public UserRestData adaptToRestWithPrivateData(User user) {
		return this.adaptToRest(user, true);
	}
	
	private UserRestData adaptToRest(User user, Boolean includePrivateData) {
		return modelToRest.adapt(user, includePrivateData);
	}
	
	public List<UserRestData> adaptToRest(List<User> users) {
		return modelToRest.adapt(users);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.restToModel = new UserRestToUserAdapter();
		this.modelToRest = new UserToUserRestAdapter();
	}
	
}
