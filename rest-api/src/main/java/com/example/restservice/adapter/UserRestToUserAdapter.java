package com.example.restservice.adapter;

import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRestInput;

public class UserRestToUserAdapter {
	
	public UserRestToUserAdapter() {
		
	}
	
	public User adapt(UserRestInput request) {
		User user = new User();
		if(request != null) {
			user.setId(request.getId());
			user.setAvatar(request.getAvatar());
			user.setBirthDate(request.getBirthDate());
			user.setEmail(request.getEmail());
			user.setFirstname(request.getFirstname());
			user.setSurname(request.getSurname());
		}
		return user;
	}

}
