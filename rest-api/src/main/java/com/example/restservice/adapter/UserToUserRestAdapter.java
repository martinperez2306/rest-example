package com.example.restservice.adapter;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.user.User;
import com.example.restservice.model.user.UserRestData;

public class UserToUserRestAdapter {
	
	public UserToUserRestAdapter() {
		
	}
	
	public List<UserRestData> adapt(List<User> users) {
		List<UserRestData> usersAdapted = new ArrayList<UserRestData>();
		if(users != null) {
			for (User user : users) {
				usersAdapted.add(adapt(user));
			}
		}
		return usersAdapted;
	}
	
	public List<UserRestData> adapt(List<User> users, Boolean includePrivateData){
		List<UserRestData> usersAdapted = new ArrayList<UserRestData>();
		if(users != null) {
			for (User user : users) {
				usersAdapted.add(adapt(user, includePrivateData));
			}
		}
		return usersAdapted;
	}
	
	public UserRestData adapt(User user) {
		UserRestData userApi = adaptUserToUserApi(user, false);
		return userApi;
	}
	
	public UserRestData adapt(User user, Boolean includePrivateData) {
		UserRestData userApi = adaptUserToUserApi(user, includePrivateData);
		return userApi;
	}
	
	private UserRestData adaptUserToUserApi(User user, Boolean includePrivateData) {
		UserRestData userApi = new UserRestData();
		userApi.setId(user.getId());
		if(includePrivateData)
			adaptPrivateData(userApi, user);
		adaptPublicData(userApi, user);
		return userApi;
	}
	
	private UserRestData adaptPrivateData(UserRestData data, User user) {
		data.setBirthDate(user.getBirthDate());
		data.setEmail(user.getEmail());
		data.setConfirmedAccount(user.isVerified());
		return data;
	}
	
	private UserRestData adaptPublicData(UserRestData data, User user) {
		data.setAvatar(user.getAvatar());
		data.setFirstname(user.getFirstname());
		data.setSurname(user.getSurname());
		return data;
	}
	
}
