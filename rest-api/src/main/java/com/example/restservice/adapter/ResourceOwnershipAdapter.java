package com.example.restservice.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ResourceOwnership;
import com.example.restservice.model.resource.Action;
import com.example.restservice.model.resource.ResourceRestData;

@Component
public class ResourceOwnershipAdapter {
	
	@Autowired
	private ResourceAdapter resourceAdapter;
	
	public ResourceRestData adaptToRest(ResourceOwnership resourceOwnership) {
		ResourceRestData restData = resourceAdapter.adaptToRest(resourceOwnership.getResource());
		restData.setOwnership(resourceOwnership.getOwnership().getOwnership());
		restData.setActions(adaptPrivilegeToActions(resourceOwnership.getPrivilegies()));
		return restData;
	}

	public List<ResourceRestData> adaptToRest(List<ResourceOwnership>  resourceOwnerships) {
		List<ResourceRestData> restData = new ArrayList<ResourceRestData>();
		for (ResourceOwnership resourceOwnership : resourceOwnerships) {
			restData.add(this.adaptToRest(resourceOwnership));
		}
		return restData;
	}
	
	private List<Action> adaptPrivilegeToActions(List<Privilege> privilegies){
		return privilegies.stream().map(p -> p.getAction()).collect(Collectors.toList());
	}

}
