package com.example.restservice.model.response;

import com.example.restservice.model.resource.ResourceRestData;

public class ResourceResponse extends Response {
	
	private ResourceRestData data;
	
	public ResourceResponse() {
		super();
	}

	public ResourceRestData getData() {
		return data;
	}

	public void setData(ResourceRestData data) {
		this.data = data;
	}

}
