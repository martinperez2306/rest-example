package com.example.restservice.model.user;

public interface AccountState {
	
	public Boolean isVerified();

	public AccountState confirm();

}
