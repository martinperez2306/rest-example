package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.Action;

public interface Privilege {
	
	public void setNextPrivilege(Privilege privilege);

	public void read() throws ForbidenException;
	
	public void write() throws ForbidenException;
	
	public void delete() throws ForbidenException;

	public List<Privilege> getPrivileges();

	public Boolean isLastPrivilege();
	
	public Action getAction();

}
