package com.example.restservice.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.access.AccessRequestRestData;

public class AccessRequestCollectionResponse extends Response{
	
	private List<AccessRequestRestData> data;
	
	public AccessRequestCollectionResponse() {
		super();
		data = new ArrayList<AccessRequestRestData>();
	}

	public List<AccessRequestRestData> getData() {
		return data;
	}

	public void setData(List<AccessRequestRestData> data) {
		this.data = data;
	}

}
