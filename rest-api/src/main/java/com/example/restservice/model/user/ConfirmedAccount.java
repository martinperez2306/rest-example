package com.example.restservice.model.user;

public class ConfirmedAccount implements AccountState{

	@Override
	public Boolean isVerified() {
		return true;
	}

	@Override
	public AccountState confirm() {
		return new ConfirmedAccount();
	}

}
