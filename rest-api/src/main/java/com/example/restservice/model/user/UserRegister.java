package com.example.restservice.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Esta clase es responsable de modelar
 * los datos necesarios para el registro
 * del Usuario.
 * @author Martin Perez
 *
 */
public class UserRegister {
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("userdata")
	private UserRestInput userdata;
	
	public UserRegister() {
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserRestInput getUserData() {
		return userdata;
	}

	public void setUserData(UserRestInput userRest) {
		this.userdata = userRest;
	}

}
