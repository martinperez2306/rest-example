package com.example.restservice.model.resource;

public class Action {
	
	private String displayName;
	private String actionName;
	
	public Action(String displayName, String actionName) {
		this.displayName = displayName;
		this.actionName = actionName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getActionName() {
		return actionName;
	}

}
