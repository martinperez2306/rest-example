package com.example.restservice.model.response;

import com.example.restservice.model.user.UserRestData;

/**
 * Clase responsable de modelar la respuesta
 * de los datos de un Usuario.
 * @author Martin Perez
 *
 */
public class UserResponse extends Response {
	
	private UserRestData data;
	
	public UserResponse() {
		super();
	}

	public UserRestData getData() {
		return data;
	}

	public void setData(UserRestData data) {
		this.data = data;
	}

}
