package com.example.restservice.model.user;

import java.util.Date;
import java.util.List;

/**
 * Clase responsable de encapsular la informacion
 * privada del Usuario
 * @author Martin Perez
 *
 */
public class PrivateUserData {

	private String email;
	private Date birthDate;
	private List<String> wishList;
	
	public PrivateUserData() {
		email = "";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public List<String> getWishList() {
		return wishList;
	}

	public void setWishList(List<String> wishList) {
		this.wishList = wishList;
	}
	
}
