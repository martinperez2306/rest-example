package com.example.restservice.model.invitation;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.restservice.repository.InvitationRepository;

@Component
public class InvitationScheduler implements InitializingBean{
	
	@Autowired
	private InvitationRepository invitationRepository;
	
	@Scheduled(fixedRateString ="${invitation.expiration.rate:600000}")
	public void removeExpiredInvitations() {
		System.out.println("Borrando invitaciones expiradas");
		invitationRepository.deleteExpiredInvitations();
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
	}

}
