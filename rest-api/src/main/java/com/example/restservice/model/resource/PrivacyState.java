package com.example.restservice.model.resource;

public interface PrivacyState {
	
	public Boolean isVisible();
	public ResourcePrivacy getPrivacy();

}
