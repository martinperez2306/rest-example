package com.example.restservice.model.user;

/**
 * Clase responsable de encapsular la informacion
 * publica del usuario.
 * @author Martin Perez
 *
 */
public class PublicUserData {
	
	private String firstname;
	private String surname;
	private String avatar;
	
	public PublicUserData() {
		firstname = "";
		surname = "";
		avatar = "";
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
