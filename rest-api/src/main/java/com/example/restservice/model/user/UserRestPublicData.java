package com.example.restservice.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase responsable de encapsular la informacion
 * publica del usuario para los servicios REST.
 * @author Martin Perez
 *
 */
public class UserRestPublicData {
	
	@JsonProperty("firstname")
	private String firstname;
	@JsonProperty("surname")
	private String surname;
	
	@JsonProperty("avatar")
	private String avatar;
	
	public UserRestPublicData() {
		firstname = "";
		surname = "";
		avatar = "";
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
