package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;

public interface PrivilegeChain {
	
	public PrivilegeChain nextPrivilege(Privilege privilege);
	
	public List<Privilege> getPrivileges();
	
	public void doRead() throws ForbidenException;

	public void doWrite() throws ForbidenException;
	
	public void doDelete() throws ForbidenException;

}
