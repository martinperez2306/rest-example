package com.example.restservice.model.access;

public class AccessRequestRestData {
	
	private Long userId;
	private Long resourceId;

	public AccessRequestRestData(Long userId, Long resourceId) {
		this.resourceId = resourceId;
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

}
