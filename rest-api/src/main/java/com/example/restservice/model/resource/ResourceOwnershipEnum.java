package com.example.restservice.model.resource;

public enum ResourceOwnershipEnum {
	
	OWNER, INVITE, NO_OWNERSHIP;

}
