package com.example.restservice.model.resource;

public class Resource {
	
	private Long id;
	private String name;
	private String detail;
	private PrivacyState privacy;
	
	public Resource() {
		this.privacy = new PublicState();
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public void setAsPrivate() {
		this.privacy = new PrivateState();
	}
	
	public void setAsPublic() {
		this.privacy = new PublicState();
	}
	
	public Boolean isVisible() {
		return privacy.isVisible();
	}

	

}
