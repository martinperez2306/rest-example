package com.example.restservice.model.privileges;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.AcceptInvitationAction;
import com.example.restservice.model.resource.Action;

public class AcceptInvitationPrivilege implements Privilege {
	
	private Privilege nextPrivilege;
	
	public AcceptInvitationPrivilege() {
		nextPrivilege = new NoPrivilege();
	}

	@Override
	public void setNextPrivilege(Privilege privilege) {
		if(nextPrivilege.isLastPrivilege())
			nextPrivilege = privilege;
		else
			nextPrivilege.setNextPrivilege(privilege);
	}

	@Override
	public void read() throws ForbidenException {
		nextPrivilege.read();
	}

	@Override
	public void write() throws ForbidenException {
		nextPrivilege.write();
	}

	@Override
	public void delete() throws ForbidenException {
		nextPrivilege.delete();
	}

	@Override
	public List<Privilege> getPrivileges() {
		List<Privilege> privileges = new ArrayList<Privilege>();
		privileges.addAll(nextPrivilege.getPrivileges());
		privileges.add(new AcceptInvitationPrivilege());
		return privileges;
	}

	@Override
	public Boolean isLastPrivilege() {
		return false;
	}

	@Override
	public Action getAction() {
		return new AcceptInvitationAction();
	}

}
