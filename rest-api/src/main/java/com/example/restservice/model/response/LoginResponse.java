package com.example.restservice.model.response;

/**
 * Esta clase es responsable de modelar la
 * respuesta del Login.
 * @author Martin Perez
 *
 */
public class LoginResponse extends Response{
	
	private String token;
	
	public LoginResponse() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	

}
