package com.example.restservice.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.user.UserRestData;

/**
 * Clase responsable de modelar la respuesta
 * de los datos de una coleccion de Usuarios.
 * @author Martin Perez
 *
 */
public class UserCollectionResponse extends Response{
	
	private List<UserRestData> data;
	
	public UserCollectionResponse() {
		super();
		data = new ArrayList<UserRestData>();
	}

	public List<UserRestData> getData() {
		return data;
	}

	public void setData(List<UserRestData> data) {
		this.data = data;
	}
	
}
