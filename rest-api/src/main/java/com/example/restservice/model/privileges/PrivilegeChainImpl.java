package com.example.restservice.model.privileges;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.exception.ForbidenException;

public class PrivilegeChainImpl implements PrivilegeChain{
	
	private Privilege firstChain;
	
	public PrivilegeChainImpl(Privilege firtsChain) {
		this.firstChain = firtsChain;
	}
	
	public PrivilegeChain nextPrivilege(Privilege privilege) {
		this.firstChain.setNextPrivilege(privilege);
		return this;
	}
	
	public void doRead() throws ForbidenException {
		firstChain.read();
	}

	public void doWrite() throws ForbidenException{
		firstChain.write();
	}
	
	@Override
	public void doDelete() throws ForbidenException {
		firstChain.delete();
	}

	@Override
	public List<Privilege> getPrivileges() {
		List<Privilege> privileges = new ArrayList<Privilege>();
		privileges.addAll(firstChain.getPrivileges());
		return privileges;
	}

}
