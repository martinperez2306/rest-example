package com.example.restservice.model.resource;

public class PrivateState implements PrivacyState{

	@Override
	public Boolean isVisible() {
		return false;
	}

	@Override
	public ResourcePrivacy getPrivacy() {
		return ResourcePrivacy.PRIVATE;
	}
	
}
