package com.example.restservice.model.invitation;

public class InvitationRestData {
	
	private Long userId;
	private Long resourceId;
	private String code;
	
	public InvitationRestData(Long userId, Long resourceId, String code) {
		this.userId = userId;
		this.resourceId = resourceId;
		this.code = code;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getResourceId() {
		return resourceId;
	}
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

}
