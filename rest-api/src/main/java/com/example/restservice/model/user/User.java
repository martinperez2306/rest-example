package com.example.restservice.model.user;

import java.util.Date;

/**
 * Clase responsable de modelar la informacion
 * del Usuario.
 * @author Martin Perez
 *
 */
public class User {
	
	private Long id;
	
	private String username;
	private String password;
	
	private PublicUserData publicUserData;
	private PrivateUserData privateUserData;
	
	private AccountState accountState;
	
	public User() {
		publicUserData = new PublicUserData();
		privateUserData = new PrivateUserData();
		accountState = new UnconfirmedAccount();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return privateUserData.getEmail();
	}

	public void setEmail(String email) {
		this.privateUserData.setEmail(email);
	}

	public String getFirstname() {
		return publicUserData.getFirstname();
	}

	public void setFirstname(String firstname) {
		this.publicUserData.setFirstname(firstname);
	}

	public String getSurname() {
		return publicUserData.getSurname();
	}

	public void setSurname(String surname) {
		this.publicUserData.setSurname(surname);
	}

	public String getAvatar() {
		return this.publicUserData.getAvatar();
	}

	public void setAvatar(String avatar) {
		this.publicUserData.setAvatar(avatar);
	}

	public Date getBirthDate() {
		return this.privateUserData.getBirthDate();
	}

	public void setBirthDate(Date birthDate) {
		this.privateUserData.setBirthDate(birthDate);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Boolean isVerified() {
		return this.accountState.isVerified();
	}
	
	public void confirmAccont() {
		this.accountState = this.accountState.confirm();
	}

}
