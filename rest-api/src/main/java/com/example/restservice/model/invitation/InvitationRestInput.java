package com.example.restservice.model.invitation;

import java.util.List;

public class InvitationRestInput {

	private List<String> actions;
	
	public InvitationRestInput() {
		
	}

	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}
	
}
