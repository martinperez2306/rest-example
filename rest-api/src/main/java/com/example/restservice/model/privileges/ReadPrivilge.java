package com.example.restservice.model.privileges;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.Action;
import com.example.restservice.model.resource.ReadAction;

public class ReadPrivilge implements Privilege{
	
	private Privilege nextPrivilege;
	
	public ReadPrivilge() {
		this.nextPrivilege = new NoPrivilege();
	}
	
	@Override
	public void setNextPrivilege(Privilege privilege) {
		if(nextPrivilege.isLastPrivilege())
			nextPrivilege = privilege;
		else
			nextPrivilege.setNextPrivilege(privilege);
	}

	@Override
	public void read() throws ForbidenException{
		return;
	}

	@Override
	public void write() throws ForbidenException{
		nextPrivilege.write();
	}
	
	@Override
	public List<Privilege> getPrivileges() {
		List<Privilege> privileges = new ArrayList<Privilege>();
		privileges.addAll(nextPrivilege.getPrivileges());
		privileges.add(new ReadPrivilge());
		return privileges;
	}

	@Override
	public Boolean isLastPrivilege() {
		return false;
	}

	@Override
	public void delete() throws ForbidenException {
		nextPrivilege.delete();
	}

	@Override
	public Action getAction() {
		return new ReadAction();
	}

}
