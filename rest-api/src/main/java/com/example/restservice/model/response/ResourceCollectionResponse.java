package com.example.restservice.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.resource.ResourceRestData;

public class ResourceCollectionResponse extends Response {
	
	private List<ResourceRestData> data;
	
	public ResourceCollectionResponse() {
		super();
		data = new ArrayList<ResourceRestData>();
	}

	public List<ResourceRestData> getData() {
		return data;
	}

	public void setData(List<ResourceRestData> data) {
		this.data = data;
	}

}
