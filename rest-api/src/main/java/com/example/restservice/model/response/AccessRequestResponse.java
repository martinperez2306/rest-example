package com.example.restservice.model.response;

import com.example.restservice.model.access.AccessRequestRestData;

public class AccessRequestResponse extends Response{
	
	private AccessRequestRestData data;
	
	public AccessRequestResponse() {
		super();
	}

	public AccessRequestRestData getData() {
		return data;
	}

	public void setData(AccessRequestRestData data) {
		this.data = data;
	}

}
