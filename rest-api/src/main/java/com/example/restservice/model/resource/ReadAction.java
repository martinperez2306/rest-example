package com.example.restservice.model.resource;

import com.example.restservice.model.resource.Action;

public class ReadAction extends Action {

	public ReadAction() {
		super("Ver", "READ");
	}

}
