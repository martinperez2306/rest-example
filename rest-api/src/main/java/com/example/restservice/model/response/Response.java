package com.example.restservice.model.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase tiene la responsabilidad de modelar 
 * una respuesta generica que devuelve la API REST.
 * 
 * Todas las clases que sean una Respuesta de la API
 * heredaran de esta para poder proveer de un Status 
 * y una Lista de errores informativos.
 * @author Martin Perez
 *
 */
public class Response {
	
	private String status;
	private List<ErrorResponse> errors;
	
	public Response() {
		status = "";
		errors = new ArrayList<ErrorResponse>();
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ErrorResponse> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorResponse> errors) {
		this.errors = errors;
	}
	

}
