package com.example.restservice.model.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase responsable de modelar los datos
 * de salida de un usuario para los servicios 
 * REST.
 * @author Martin Perez
 *
 */
public class UserRestData {
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("publicUserData")
	private UserRestPublicData publicUserData;
	
	@JsonProperty("privateUserData")
	private UserRestPrivateData privateUserData;
	
	public UserRestData() {
		this.privateUserData = new UserRestPrivateData();
		this.publicUserData = new UserRestPublicData();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return privateUserData.getEmail();
	}

	public void setEmail(String email) {
		this.privateUserData.setEmail(email);
	}

	public String getFirstname() {
		return publicUserData.getFirstname();
	}

	public void setFirstname(String firstname) {
		this.publicUserData.setFirstname(firstname);
	}

	public String getSurname() {
		return publicUserData.getSurname();
	}

	public void setSurname(String surname) {
		this.publicUserData.setSurname(surname);
	}

	public String getAvatar() {
		return this.publicUserData.getAvatar();
	}

	public void setAvatar(String avatar) {
		this.publicUserData.setAvatar(avatar);
	}

	public Date getBirthDate() {
		return this.privateUserData.getBirthDate();
	}

	public void setBirthDate(Date birthDate) {
		this.privateUserData.setBirthDate(birthDate);
	}

	public Boolean isConfirmedAccount() {
		return this.privateUserData.isConfirmedAccount();
	}
	
	public void setConfirmedAccount(Boolean confirmedAccount) {
		this.privateUserData.setConfirmedAccount(confirmedAccount);
	}

}
