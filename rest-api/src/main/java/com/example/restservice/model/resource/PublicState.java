package com.example.restservice.model.resource;

public class PublicState implements PrivacyState{

	@Override
	public Boolean isVisible() {
		return true;
	}

	@Override
	public ResourcePrivacy getPrivacy() {
		return ResourcePrivacy.PUBLIC;
	}

}
