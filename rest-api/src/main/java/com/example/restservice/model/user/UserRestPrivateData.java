package com.example.restservice.model.user;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Clase responsable de encapsular la informacion
 * privada del usuario para los servicios REST.
 * @author Martin Perez
 *
 */
public class UserRestPrivateData {
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("birthDate")
	private Date birthDate;
	
	@JsonProperty("wishList")
	private List<String> wishList;
	
	@JsonProperty("confirmedAccount")
	private Boolean confirmedAccount;
	
	public UserRestPrivateData() {
		email = "";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public List<String> getWishList() {
		return wishList;
	}

	public void setWishList(List<String> wishList) {
		this.wishList = wishList;
	}

	public Boolean isConfirmedAccount() {
		return confirmedAccount;
	}

	public void setConfirmedAccount(Boolean confirmedAccount) {
		this.confirmedAccount = confirmedAccount;
	}

}
