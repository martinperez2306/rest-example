package com.example.restservice.model.response;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.model.invitation.InvitationRestData;

public class InvitationCollectionResponse extends Response {
	
	private List<InvitationRestData> data;
	
	public InvitationCollectionResponse() {
		super();
		data = new ArrayList<InvitationRestData>();
	}

	public List<InvitationRestData> getData() {
		return data;
	}

	public void setData(List<InvitationRestData> data) {
		this.data = data;
	}

}
