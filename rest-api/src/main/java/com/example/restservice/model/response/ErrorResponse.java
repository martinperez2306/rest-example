package com.example.restservice.model.response;

/**
 * Esta clase tiene la responsabilidad de modelar
 * los errores de respuesta.
 * 
 * Ofrece un detalle del error ocurrido.
 * @author Martin Perez
 *
 */
public class ErrorResponse {
	
	private String detail;
	
	public ErrorResponse() {
		
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

}
