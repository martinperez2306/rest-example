package com.example.restservice.model.user;

public class UnconfirmedAccount implements AccountState{

	@Override
	public Boolean isVerified() {
		return false;
	}

	@Override
	public AccountState confirm() {
		return new ConfirmedAccount();
	}

}
