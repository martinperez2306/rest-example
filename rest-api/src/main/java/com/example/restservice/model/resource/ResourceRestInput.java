package com.example.restservice.model.resource;

public class ResourceRestInput {
	
	private Long id;
	private String name;
	private String detail;
	private ResourcePrivacy privacy;
	
	public ResourceRestInput() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public ResourcePrivacy getPrivacy() {
		return privacy;
	}

	public void setPrivacy(ResourcePrivacy privacy) {
		this.privacy = privacy;
	}

}
