package com.example.restservice.model.invitation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.restservice.model.privileges.Privilege;
import com.example.restservice.model.privileges.ReadPrivilge;

public class Invitation {
	
	private Long userId;
	private Long resourceId;
	private String invitationCode;
	private Date limitDate;
	private List<Privilege> privileges;
	
	public Invitation(Long userId, Long resourceId, Date limitDate) {
		this.resourceId = resourceId;
		this.userId = userId;
		this.limitDate = limitDate;
		this.privileges = new ArrayList<Privilege>();
		privileges.add(new ReadPrivilge());
	}
	
	public Invitation(Long userId, Long resourceId, Date limitDate, List<Privilege> privileges) {
		this.resourceId = resourceId;
		this.userId = userId;
		this.limitDate = limitDate;
		this.privileges = privileges;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getResourceId() {
		return resourceId;
	}
	
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
	
	public String getInvitationCode() {
		return invitationCode;
	}
	
	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}
	
	public Date getLimitDate() {
		return limitDate;
	}
	
	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}
	
	public Boolean isExpired() {
		return this.limitDate.before(new Date(System.currentTimeMillis()));
	}

	public List<Privilege> getPrivileges() {
		return this.privileges;
	}
}
