package com.example.restservice.model.response;

import org.springframework.core.io.ByteArrayResource;

public class DownloadResource extends ByteArrayResource {

	public DownloadResource(byte[] bytes) {
		super(bytes);
	}

}
