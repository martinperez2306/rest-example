package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.ResourceOwnershipEnum;

public class Invited implements Ownership {
	
	private PrivilegeChain privilegeChain;
	
	public Invited(Privilege privilege) {
		privilegeChain = new PrivilegeChainImpl(privilege);
	}
	
	public Invited(List<Privilege> privileges) {
		if(privileges != null && !privileges.isEmpty()) {
			privilegeChain = new PrivilegeChainImpl(privileges.remove(0));
			for (Privilege privilege : privileges) {
				privilegeChain.nextPrivilege(privilege);
			}
		}
	}

	@Override
	public List<Privilege> getPrivileges() {
		return privilegeChain.getPrivileges();
	}

	@Override
	public void readResource() throws ForbidenException{
		privilegeChain.doRead();
	}

	@Override
	public void writeResource() throws ForbidenException{
		privilegeChain.doWrite();
	}

	@Override
	public void deleteResource() throws ForbidenException {
		privilegeChain.doDelete();
	}

	@Override
	public ResourceOwnershipEnum getOwnership() {
		return ResourceOwnershipEnum.INVITE;
	}

	@Override
	public Ownership getWithPrivilege(List<Privilege> privileges) {
		return new Invited(privileges);
	}

}
