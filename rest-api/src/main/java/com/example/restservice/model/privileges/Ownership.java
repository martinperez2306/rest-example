package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.ResourceOwnershipEnum;

public interface Ownership {
	
	public List<Privilege> getPrivileges();

	public void readResource() throws ForbidenException;

	public void writeResource() throws ForbidenException;

	public void deleteResource() throws ForbidenException;
	
	public ResourceOwnershipEnum getOwnership();
	
	public Ownership getWithPrivilege(List<Privilege> privileges);

}
