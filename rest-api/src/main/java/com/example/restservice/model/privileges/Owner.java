package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.ResourceOwnershipEnum;

public class Owner implements Ownership {
	
	private PrivilegeChain privilegeChain;
	
	public Owner() {
		this.privilegeChain = new PrivilegeChainImpl(new ReadPrivilge());
		this.privilegeChain.nextPrivilege(new WritePrivilege())
						   .nextPrivilege(new DeletePrivilege())
						   .nextPrivilege(new AddMemberPrivilege())
						   .nextPrivilege(new DeleteMemberPrivilege());
	}

	@Override
	public List<Privilege> getPrivileges() {
		return privilegeChain.getPrivileges();
	}

	@Override
	public void readResource() throws ForbidenException {
		privilegeChain.doRead();
	}

	@Override
	public void writeResource() throws ForbidenException {
		privilegeChain.doWrite();
	}

	@Override
	public void deleteResource() throws ForbidenException {
		privilegeChain.doDelete();
	}

	@Override
	public ResourceOwnershipEnum getOwnership() {
		return ResourceOwnershipEnum.OWNER;
	}

	@Override
	public Ownership getWithPrivilege(List<Privilege> privileges) {
		return new Owner();
	}

}
