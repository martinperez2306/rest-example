package com.example.restservice.model.privileges;

import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;

public class ResourceOwnership {
	
	private Long id;
	private User user;
	private Resource resource;
	private Ownership ownership;
	
	public ResourceOwnership(User user, Resource resource, Ownership ownership) {
		this.user = user;
		this.resource = resource;
		this.ownership = ownership;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}
	
	public Resource getResource() {
		return resource;
	}
	
	public Ownership getOwnership() {
		return ownership;
	}
	
	public Resource readResource() throws ForbidenException {
		ownership.readResource();
		return getResource();
	}

	public Resource writeResource(Resource resourceUpdated) throws ForbidenException {
		ownership.writeResource();
		return resourceUpdated;
	}

	public void deleteResource() throws ForbidenException{
		ownership.deleteResource();
	}
	
	public List<Privilege> getPrivilegies(){
		return ownership.getPrivileges();
	}
}
