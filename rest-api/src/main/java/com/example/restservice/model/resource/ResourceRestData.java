package com.example.restservice.model.resource;

import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResourceRestData {
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("detail")
	private String detail;
	
	@JsonProperty("file")
	private InputStream file;
	
	@JsonProperty("privacy")
	private ResourcePrivacy privacy;
	
	@JsonProperty("ownership")
	private ResourceOwnershipEnum ownership;
	
	@JsonProperty("actions")
	private List<Action> actions;
	
	public ResourceRestData() {
		this.privacy = ResourcePrivacy.PUBLIC;
		this.ownership = ResourceOwnershipEnum.NO_OWNERSHIP;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public InputStream getFile() {
		return file;
	}

	public void setFile(InputStream file) {
		this.file = file;
	}

	public ResourcePrivacy getPrivacy() {
		return privacy;
	}

	public void setPrivacy(ResourcePrivacy privacy) {
		this.privacy = privacy;
	}

	public ResourceOwnershipEnum getOwnership() {
		return ownership;
	}

	public void setOwnership(ResourceOwnershipEnum ownership) {
		this.ownership = ownership;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	
	public List<Action> getActions(){
		return actions;
	}

}
