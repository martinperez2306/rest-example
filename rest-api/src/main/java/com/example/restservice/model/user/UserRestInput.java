package com.example.restservice.model.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase responsable de modelar los datos
 * de entrada de un usuario para los servicios 
 * REST.
 * @author Martin Perez
 *
 */
public class UserRestInput {
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("firstname")
	private String firstname;
	
	@JsonProperty("surname")
	private String surname;
	
	@JsonProperty("avatar")
	private String avatar;
	
	@JsonProperty("birthDate")
	private Date birthDate;
	
	public UserRestInput() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

}
