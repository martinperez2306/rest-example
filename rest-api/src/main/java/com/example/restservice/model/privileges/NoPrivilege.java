package com.example.restservice.model.privileges;

import java.util.ArrayList;
import java.util.List;

import com.example.restservice.exception.ForbidenException;
import com.example.restservice.model.resource.Action;
import com.example.restservice.model.response.ErrorResponse;

public class NoPrivilege implements Privilege {

	@Override
	public void setNextPrivilege(Privilege privilege) {

	}

	@Override
	public void read() throws ForbidenException {
		throw new ForbidenException(this.generateError("Access Denied: Read resource"));
	}

	@Override
	public void write() throws ForbidenException {
		throw new ForbidenException(this.generateError("Access Denied: Write resource"));
	}
	
	private List<ErrorResponse> generateError(String message){
		ErrorResponse error = new ErrorResponse();
		error.setDetail(message);
		List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
		errors.add(error);
		return errors;
	}
	
	@Override
	public List<Privilege> getPrivileges() {
		List<Privilege> privileges = new ArrayList<Privilege>();
		return privileges;
	}
	
	@Override
	public Boolean isLastPrivilege() {
		return true;
	}

	@Override
	public void delete() throws ForbidenException {
		throw new ForbidenException(this.generateError("Access Denied: Delete resource"));
	}

	@Override
	public Action getAction() {
		return null;
	}

}
