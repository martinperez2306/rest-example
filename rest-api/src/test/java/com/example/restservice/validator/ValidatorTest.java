package com.example.restservice.validator;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.example.restservice.model.response.ErrorResponse;
import com.example.restservice.model.user.User;

@SpringBootTest
public class ValidatorTest {
	
	@Autowired
	@Qualifier("credentialsValidator")
	private Validator credentialsValidator;
	
	@Autowired
	@Qualifier("userValidator")
	private Validator userValidator;
	
	@Test
	public void userValidatorTestMustReturnErrorIfUserIsNull() {
		User user = null;
		
		List<ErrorResponse> errorsUser = userValidator.validate(user);
		
		Assert.isTrue(errorsUser.size() == 1,"");
	}
	
	@Test
	public void emailValidatorTestMustReturnErrosIfEmailIsNullOrEmpty() {
		User user = new User();
		user.setAvatar("Avatar");
		user.setBirthDate(new Date());
		user.setFirstname("Firstname");
		user.setPassword("Password");
		user.setSurname("Surname");
		user.setUsername("Username");
		Validator validator = new EmailValidator();
		List<ErrorResponse> errors =  validator.validate(user);
		Assert.notEmpty(errors, "");
		Assert.isTrue(errors.size() == 1,"");
	}
	
	@Test
	public void nameValidatorTestMustReturnErrosIfNameIsNullOrEmpty() {
		User user = new User();
		user.setAvatar("Avatar");
		user.setBirthDate(new Date());
		user.setPassword("Password");
		user.setUsername("Username");
		user.setEmail("Email");
		Validator validator = new NameValidator();
		List<ErrorResponse> errors =  validator.validate(user);
		Assert.notEmpty(errors, "");
		Assert.isTrue(errors.size() == 2,"");
	}
	
	@Test
	public void dateValidatorTestMustReturnErrosIfDateIsNullOrEmpty() {
		User user = new User();
		user.setAvatar("Avatar");
		user.setFirstname("Firstname");
		user.setPassword("Password");
		user.setSurname("Surname");
		user.setUsername("Username");
		user.setEmail("Email");
		Validator validator = new DateValidator();
		List<ErrorResponse> errors =  validator.validate(user);
		Assert.notEmpty(errors, "");
		Assert.isTrue(errors.size() == 1,"");
	}
	
	@Test
	public void credentialdsValidatorTestMustReturnErrosIfCredentialslAreNullOrEmpty() {
		User user = new User();
		user.setAvatar("Avatar");
		user.setBirthDate(new Date());
		user.setFirstname("Firstname");
		user.setSurname("Surname");
		user.setEmail("Email");
		List<ErrorResponse> errors =  credentialsValidator.validate(user);
		Assert.notEmpty(errors, "");
		Assert.isTrue(errors.size() == 2,"");
	}

}
