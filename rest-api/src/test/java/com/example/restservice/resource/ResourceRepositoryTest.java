package com.example.restservice.resource;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;
import com.example.restservice.repository.ResourceRepository;

@SpringBootTest
public class ResourceRepositoryTest {
	
	@Autowired
	private ResourceRepository resourceRepository;
	
	@BeforeEach
	public void initRepository() throws Exception {
		InitializingBean bean = (InitializingBean) resourceRepository;
		bean.afterPropertiesSet();
	}
	
	private Resource fillResource(String name, String detail) {
		Resource resource = new Resource();
		resource.setName(name);
		resource.setDetail(detail);
		return resource;
	}
	
	@Test
	public void addResourceToRepositoryTest() {
		Resource resource = this.fillResource("Name","Detail");
		Assert.isNull(resource.getId(), "Resource must be created without ID");
		resource = resourceRepository.insert(resource);
		Assert.notNull(resource.getId(), "Insert add resource ID");
	}
	
	@Test
	public void selectResourceToRepositoryTest() {
		Resource resource = this.fillResource("Name","Detail");
		resource = resourceRepository.insert(resource);
		Resource resourceFinded = resourceRepository.select(resource.getId());
		
		Assert.notNull(resource.getId(), "Resource was not found");
		Assert.isTrue(resourceFinded.getId() == resource.getId(), "Ids do not match");
		Assert.isTrue(resourceFinded.getName().equals(resource.getName()), "Names do not match");
		Assert.isTrue(resourceFinded.getDetail().equals(resource.getDetail()), "Details do not match");
		Assert.isTrue(resourceFinded.isVisible() && resource.isVisible(), "Privacy do not match");
	}
	
	@Test
	public void updateResourceToRepositoryTest() {
		Resource resource = this.fillResource("Name","Detail");
		resource = resourceRepository.insert(resource);
		
		Resource resourceUpdated = this.fillResource("Updated", "Updated");
		resourceUpdated.setId(resource.getId());
		resourceUpdated.setAsPrivate();
		resourceUpdated = resourceRepository.update(resource.getId(), resourceUpdated);
		
		Assert.notNull(resource.getId(), "Resource was not found");
		Assert.isTrue(resourceUpdated.getId() == resource.getId(), "Ids do not match");
		Assert.isTrue(!resourceUpdated.getName().equals(resource.getName()), "Names match");
		Assert.isTrue(!resourceUpdated.getDetail().equals(resource.getDetail()), "Details match");
		Assert.isTrue(!resourceUpdated.isVisible() && resource.isVisible(), "Privacy match");
	}
	
	@Test
	public void deleteResourceToRepositoryTest() {
		Resource resource = this.fillResource("Name","Detail");
		resource = resourceRepository.insert(resource);
		
		resourceRepository.delete(resource.getId());
		Resource resourceFinded = resourceRepository.select(resource.getId());
		
		Assert.isNull(resourceFinded.getId(), "Resource was not delete");
	}
	
	@Test
	public void selectResourceByPrivacyTest() {
		List<Resource> oldPrivates = resourceRepository.selectByPrivacy(ResourcePrivacy.PRIVATE);
		List<Resource> oldPublics = resourceRepository.selectByPrivacy(ResourcePrivacy.PUBLIC);
		
		Resource privateResource1 = this.fillResource("Private1","Private1");
		privateResource1.setAsPrivate();
		privateResource1 = resourceRepository.insert(privateResource1);
		
		Resource privateResource2 = this.fillResource("Private2","Private2");
		privateResource2.setAsPrivate();
		privateResource2 = resourceRepository.insert(privateResource2);
		
		Resource publicResource = this.fillResource("Public","Public");
		publicResource.setAsPublic();
		publicResource = resourceRepository.insert(publicResource);
		
		List<Resource> privates = resourceRepository.selectByPrivacy(ResourcePrivacy.PRIVATE);
		List<Resource> publics = resourceRepository.selectByPrivacy(ResourcePrivacy.PUBLIC);
		
		Assert.notEmpty(privates, "Privates are empty");
		Assert.notEmpty(publics, "Publics are empty");
		
		Assert.isTrue(privates.size() == oldPrivates.size() + 2, "Private should have 2 elemtents");
		Assert.isTrue(publics.size() == oldPublics.size() + 1, "Public should have 2 elemtents");
	}

}
