package com.example.restservice.resource;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.example.restservice.exception.ApiException;
import com.example.restservice.exception.ForbidenException;
import com.example.restservice.exception.NotFoundException;
import com.example.restservice.model.access.AccessRequest;
import com.example.restservice.model.invitation.Invitation;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.user.User;
import com.example.restservice.service.ResourceService;
import com.example.restservice.service.UserService;

@SpringBootTest
public class ResourceServiceTest {
	
	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private UserService userService;
	
	private Long userSequence = 0L;
	
	private Resource fillResource(String name, String detail) {
		Resource resource = new Resource();
		resource.setName(name);
		resource.setDetail(detail);
		return resource;
	}
	
	private User fillUser(String username, String password) {
		userSequence++;
		User user = new User();
		user.setId(userSequence);
		user.setPassword(password);
		user.setUsername(username);
		return user;
	}
	
	private User fillFullUser(String username, String password, String avatar, String email, Date birthDate, String firstname, String surname) {
		User user = this.fillUser(username, password);
		user.setAvatar(avatar);
		user.setEmail(email);
		user.setBirthDate(birthDate);
		user.setFirstname(firstname);
		user.setSurname(surname);
		return user;
	}
	
	@Test
	public void createResourceTest() throws ApiException {
		Resource resource = fillResource("Resource", "Resource");
		User user = fillUser("Username", "password");
		Resource created = resourceService.createResource(resource, user);
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
	}
	
	@Test
	public void getResourceTest() throws ApiException {
		Resource resource = fillResource("Resource", "Resource");
		User user = fillUser("Username", "password");
		Resource created = resourceService.createResource(resource, user);
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		Resource finded = resourceService.getResource(created.getId(), user.getId());
		Assert.isTrue(finded.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(finded.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(finded.getDetail().equals(created.getDetail()), "Both ID do not match");
	}
	
	@Test
	public void getPublicResources() throws ApiException {
		User user1 = fillUser("User 1", "Pass 1");
		Resource resource1 = fillResource("Private 1", "Private 1");
		resource1.setAsPrivate();
		Resource resource2 = fillResource("Public 1", "Public 1");
		resource2.setAsPublic();
		
		List<Resource> oldPublicResources = resourceService.getPublicResources();
		
		resourceService.createResource(resource1, user1);
		resourceService.createResource(resource2, user1);
		
		User user2 = fillUser("User 2", "Pass 2");
		Resource resource3 = fillResource("Public 2", "Public 3");
		resource3.setAsPublic();
		Resource resource4 = fillResource("Public 3", "Public 3");
		resource4.setAsPublic();
		Resource resource5 = fillResource("Private 2", "Private 2");
		resource5.setAsPrivate();
		
		resourceService.createResource(resource3, user2);
		resourceService.createResource(resource4, user2);
		resourceService.createResource(resource5, user2);
		
		List<Resource> publicResources = resourceService.getPublicResources();
		List<Resource> user1Resources = resourceService.getResourcesForUser(user1.getId())
											.stream().map(ro -> ro.getResource()).collect(Collectors.toList());
		List<Resource> user2Resources = resourceService.getResourcesForUser(user2.getId())
											.stream().map(ro -> ro.getResource()).collect(Collectors.toList());
		
		Integer total = user1Resources.size() + user2Resources.size();
		
		Assert.isTrue(publicResources.size() == oldPublicResources.size() + 3, "");
		Assert.isTrue(user1Resources.size() == 2, "");
		Assert.isTrue(user2Resources.size() == 3, "");
		Assert.isTrue(total == 5, "");
	}
	
	@Test
	public void updateResourceTest() throws ApiException {
		Resource resource = fillResource("Resource", "Resource");
		User user = fillUser("Username", "password");
		Resource created = resourceService.createResource(resource, user);
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		Resource updated = fillResource("Update", "Update");
		updated.setAsPrivate();
		resourceService.modifyResource(created.getId(), user.getId(), updated);
		Resource finded = resourceService.getResource(created.getId(), user.getId());
		
		Assert.isTrue(finded.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(!finded.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(!finded.getDetail().equals(created.getDetail()), "Both ID do not match");
	}
	
	@Test
	public void deleteResourceTest() throws ApiException {
		Resource resource = fillResource("Resource", "Resource");
		User user = fillUser("Username", "password");
		Resource created = resourceService.createResource(resource, user);
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		resourceService.deleteResource(created.getId(), user.getId());
		
		Assertions.assertThrows(NotFoundException.class, () -> {
			resourceService.getResource(created.getId(), user.getId());
		  });
	}
	
	@Test
	public void getRequestedResources() throws ApiException {
		Resource resource = fillResource("Resource", "Resource");
		User owner = fillFullUser("owner", "owner", "avatar", "email",new Date(), "firstname", "surname");
		User requestingUser = fillFullUser("requestingUser", "requestingUser", "avatar", "email",new Date(), "firstname", "surname");
		
		owner = userService.addUser(owner);
		requestingUser = userService.addUser(requestingUser);
		Long requestingUserId = requestingUser.getId();
		
		Resource created = resourceService.createResource(resource, owner);
		Long resourceCreatedId = created.getId();
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		Assertions.assertThrows(ForbidenException.class, () -> {
			resourceService.getResource(resourceCreatedId, requestingUserId);
		  });
		
		resourceService.requestResourceAccess(created.getId(), requestingUserId);
		
		List<AccessRequest> requestedResources = resourceService.getRequestedResources(owner.getId());
		Assert.isTrue(requestedResources.size() == 1, "Both ID do not match");
		AccessRequest requestedResource = requestedResources.get(0);
		
		Assert.isTrue(requestedResource.getResourceId() == created.getId(), "Both ID do not match");
	}
	
	@Test
	public void getResourceInvitations() throws ApiException{
		Resource resource = fillResource("Resource", "Resource");
		User owner = fillFullUser("owner", "owner", "avatar", "email",new Date(), "firstname", "surname");
		User invite = fillFullUser("requestingUser", "requestingUser", "avatar", "email",new Date(), "firstname", "surname");
		
		owner = userService.addUser(owner);
		invite = userService.addUser(invite);
		Long inviteId = invite.getId();
		
		Resource created = resourceService.createResource(resource, owner);
		Long resourceCreatedId = created.getId();
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		resourceService.inviteUserToResource(inviteId, resourceCreatedId);
		
		List<Invitation> invitations = resourceService.getResourceInvitations(inviteId);
		Assert.isTrue(invitations.size() == 1, "Both ID do not match");
		Invitation invitation = invitations.get(0);
		
		Assert.isTrue(invitation.getResourceId() == created.getId(), "Both ID do not match");
	}
	
	@Test
	public void shareResourceToUser() throws ApiException{
		Resource resource = fillResource("Resource", "Resource");
		User owner = fillFullUser("owner", "owner", "avatar", "email",new Date(), "firstname", "surname");
		User invite = fillFullUser("requestingUser", "requestingUser", "avatar", "email",new Date(), "firstname", "surname");
		
		owner = userService.addUser(owner);
		invite = userService.addUser(invite);
		Long ownerId = owner.getId();
		Long inviteId = invite.getId();
		
		Resource created = resourceService.createResource(resource, owner);
		Long resourceCreatedId = created.getId();
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		Assertions.assertThrows(ForbidenException.class, () -> {
			resourceService.getResource(resourceCreatedId, inviteId);
		  });
		
		resourceService.shareResourceToUser(resourceCreatedId, ownerId, inviteId);
		
		Resource sharedResource = resourceService.getResource(resourceCreatedId, inviteId);
		
		Assert.isTrue(sharedResource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(sharedResource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(sharedResource.getDetail().equals(created.getDetail()), "Both ID do not match");
	}
	
	@Test
	public void forbidResourceToUser() throws ApiException{
		Resource resource = fillResource("Resource", "Resource");
		User owner = fillFullUser("owner", "owner", "avatar", "email",new Date(), "firstname", "surname");
		User invite = fillFullUser("requestingUser", "requestingUser", "avatar", "email",new Date(), "firstname", "surname");
		
		owner = userService.addUser(owner);
		invite = userService.addUser(invite);
		Long ownerId = owner.getId();
		Long inviteId = invite.getId();
		
		Resource created = resourceService.createResource(resource, owner);
		Long resourceCreatedId = created.getId();
		
		Assert.isTrue(resource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(resource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(resource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		Assertions.assertThrows(ForbidenException.class, () -> {
			resourceService.getResource(resourceCreatedId, inviteId);
		  });
		
		resourceService.shareResourceToUser(resourceCreatedId, ownerId, inviteId);
		
		Resource sharedResource = resourceService.getResource(resourceCreatedId, inviteId);
		
		Assert.isTrue(sharedResource.getId() == created.getId(), "Both ID do not match");
		Assert.isTrue(sharedResource.getName().equals(created.getName()), "Both ID do not match");
		Assert.isTrue(sharedResource.getDetail().equals(created.getDetail()), "Both ID do not match");
		
		resourceService.forbidResourceToUser(resourceCreatedId, ownerId, inviteId);
		
		Assertions.assertThrows(ForbidenException.class, () -> {
			resourceService.getResource(resourceCreatedId, inviteId);
		  });
	}

}
