package com.example.restservice.resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.example.restservice.adapter.ResourceAdapter;
import com.example.restservice.model.resource.Resource;
import com.example.restservice.model.resource.ResourcePrivacy;
import com.example.restservice.model.resource.ResourceRestData;
import com.example.restservice.model.resource.ResourceRestInput;

@SpringBootTest
public class ResourceTest {
	
	@Autowired
	private ResourceAdapter resourceAdapter;
	
	@Test
	public void publicResourceIsVisible() {
		Resource publicResource = new Resource();
		publicResource.setId(1L);
		publicResource.setName("Test Public");
		publicResource.setDetail("Test Public");
		
		Resource privateResource = new Resource();
		privateResource.setId(2L);
		privateResource.setName("Test Private");
		privateResource.setDetail("Test Private");
		privateResource.setAsPrivate();
		
		Assert.isTrue(publicResource.isVisible(),"Resource must be Public");
		Assert.isTrue(!privateResource.isVisible(),"Resource must be Private");
	}
	
	@Test
	public void adaptModelToRestDataTest() {
		Resource model = new Resource();
		model.setId(1L);
		model.setName("Test Name");
		model.setDetail("Test Detail");
		model.setAsPrivate();
		
		ResourceRestData restData = resourceAdapter.adaptToRest(model);
		Assert.isTrue("Test Name".equals(restData.getName()),"Name of Resource not has been adapted");
		Assert.isTrue("Test Detail".equals(restData.getDetail()),"Detail of Resource not has been adapted");
		Assert.isTrue(ResourcePrivacy.PRIVATE.equals(restData.getPrivacy()),"Privacy of Resource not has been adapted");
	}
	
	@Test
	public void adaptRestInputToModelTest() {
		ResourceRestInput restInput = new ResourceRestInput();
		restInput.setName("Test Name");
		restInput.setDetail("Test Detail");
		restInput.setPrivacy(ResourcePrivacy.PUBLIC);
		
		Resource model = resourceAdapter.adaptToModel(restInput);
		Assert.isTrue("Test Name".equals(model.getName()),"Name of Resource not has been adapted");
		Assert.isTrue("Test Detail".equals(model.getDetail()),"Detail of Resource not has been adapted");
		Assert.isTrue(model.isVisible(),"Privacy of Resource not has been adapted");
	}
	
	
}
