package com.example.restservice.token;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.example.restservice.model.user.User;
import com.example.restservice.repository.TokenRepository;

@SpringBootTest
public class TokenRepositoryTest {
	
	@Autowired
	private TokenRepository tokenRepository;
	
	private User fillUser() {
		User user = new User();
		user.setId(1L);
		user.setUsername("Username");
		return user;
	}
	
	@Test
	public void addTokenToRepositoryTest() {
		Long userId = tokenRepository.selectUserWithToken("tokenUser");
		Assert.isNull(userId,"");
		User user = this.fillUser();
		tokenRepository.insertTokenForUser("tokenUser", user.getId());
		Assert.notNull(tokenRepository.selectUserWithToken("tokenUser"), "");
	}
	
	@Test
	public void deleteResourceToRepositoryTest() {
		Long userId  = tokenRepository.selectUserWithToken("tokenUser");
		Assert.isNull(userId,"");
		User user = this.fillUser();
		tokenRepository.insertTokenForUser("tokenUser", user.getId());
		Assert.notNull(tokenRepository.selectUserWithToken("tokenUser"), "");
		tokenRepository.deleteTokenUser("tokenUser");
		userId = tokenRepository.selectUserWithToken("tokenUser");
		Assert.isNull(userId,"");
	}

}
