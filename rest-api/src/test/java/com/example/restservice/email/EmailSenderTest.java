package com.example.restservice.email;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.restservice.mail.EmailService;

@SpringBootTest
public class EmailSenderTest {
	
	@Autowired
	private EmailService emailService;
	
	@Test
	public void sendSimpleTest() {
		emailService.sendSimpleMessage("perezmartincontacto@gmail.com", "Test", "Texto de prueba!");
	}

}
