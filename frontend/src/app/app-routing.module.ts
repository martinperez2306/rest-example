import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ResourceComponent } from './components/resource/resource.component';
import { PublicComponent } from './components/public/public.component';
import { InvitationComponent } from './components/invitation/invitation.component';
import { AccessComponent } from './components/access/access.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'resource', component: ResourceComponent },
  { path: 'public', component: PublicComponent },
  { path: 'invitation', component: InvitationComponent },
  { path: 'access', component: AccessComponent },
  { path: '**',   redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
