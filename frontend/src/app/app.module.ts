import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { LoginComponent, LoginDialogComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';

//ANGULAR MATERIAL//
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { ProfileComponent, ProfileDialogComponent } from './components/profile/profile.component';
import { ResourceComponent, ResourceDialogComponent, ShareResourceDialogComponent, RemoveUserDialogComponent } from './components/resource/resource.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { PublicComponent } from './components/public/public.component';
import { InvitationComponent } from './components/invitation/invitation.component';
import { AccessComponent } from './components/access/access.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginDialogComponent,
    ProfileDialogComponent,
    ResourceDialogComponent,
    ShareResourceDialogComponent,
    RemoveUserDialogComponent,
    ProfileComponent,
    ResourceComponent,
    NavBarComponent,
    PublicComponent,
    InvitationComponent,
    AccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatInputModule,
    MatTableModule,
    MatDialogModule,
    FormsModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    MatGridListModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [LoginDialogComponent,
                    ProfileDialogComponent,
                    ResourceDialogComponent,
                    ShareResourceDialogComponent,
                    RemoveUserDialogComponent]
})
export class AppModule { }
