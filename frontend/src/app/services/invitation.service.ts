import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from './api-service';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  constructor(private tokenService: TokenService, private httpService: HttpService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': this.tokenService.get()
    }),
    observe: 'response',
    whithCredentials: false 
  };

  getInvitations(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.RESOURCES + "/invitations", this.httpOptions);
  }

  inviteUserToResource(resourceId: number, userId: number, privileges: any): Observable<HttpResponse<any>>{
    return this.httpService.doPost(API.RESOURCES + "/" + resourceId + "/invitations" + "/" + userId, privileges, this.httpOptions);
  }

}
