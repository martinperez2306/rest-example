import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private route: ActivatedRoute) { }

  store(token: string){
    localStorage.setItem('token', token);
    //localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  }

  remove() {
    localStorage.removeItem("token");
  }

  get(){
    return localStorage.getItem('token');
  }

  public isLoggedIn() {
    //return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
      //return !this.isLoggedIn();
  }

  getExpiration() {
      const expiration = localStorage.getItem("expires_at");
      const expiresAt = JSON.parse(expiration);
      //return moment(expiresAt);
  }   
}
