export const API = {
    USERS:"users",
    LOGIN: "login",
    PROFILE: "users/profile",
    RESOURCES:"resources",
    INVITATIONS: "invitations"
}