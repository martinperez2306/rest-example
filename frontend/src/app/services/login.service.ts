import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { API } from './api-service';
import { UserCredentials } from '../model/user-credentials';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpService: HttpService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    observe: 'response',
    whithCredentials: false 
  };

  login(body: UserCredentials): Observable<HttpResponse<any>>{
    return this.httpService.doPost(API.LOGIN, body,this.httpOptions);
  }
}
