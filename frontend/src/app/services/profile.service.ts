import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from './api-service';
import { TokenService } from './token.service';
import { UserInput } from '../model/user-input';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private tokenService: TokenService, private httpService: HttpService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': this.tokenService.get()
    }),
    observe: 'response',
    whithCredentials: false 
  };

  getProfile(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.PROFILE, this.httpOptions);
  }

  updateProfile(id: number, updated: UserInput): Observable<HttpResponse<any>>{
    return this.httpService.doPut(API.PROFILE, updated,this.httpOptions);
  }
}
