import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {
    console.log(environment.backendUrl);
   }

  doGet(url: string, options: any): Observable<any>{
    return this.httpClient.get(environment.backendUrl + url, options)
  }

  doPost(url: string, body: any ,options: any): Observable<any>{
    return this.httpClient.post(environment.backendUrl + url, body, options);
  }

  doPostWithFile(url: string, body: any, file: File, options: any): Observable<any>{
    const formData: FormData = new FormData();
    console.log(body);
    formData.append('file', file);
    formData.append('resource', JSON.stringify(body));

    return this.httpClient.post(environment.backendUrl + url, formData, options);
  }

  doPut(url: string, body: any, options: any): Observable<any>{
    return this.httpClient.put(environment.backendUrl + url, body, options);
  }

  doPutWithFile(url: string, body: any, file: File, options: any): Observable<any>{
    const formData: FormData = new FormData();
    console.log(body);
    formData.append('file', file);
    formData.append('resource', JSON.stringify(body));

    return this.httpClient.put(environment.backendUrl + url, formData, options);
  }

  doDelete(url: string, options:any): Observable<any>{
    return this.httpClient.delete(environment.backendUrl + url, options);
  }

  doPatch(url: string, options:any): Observable<any>{
    return this.httpClient.patch(environment.backendUrl + url, options);
  }
}
