import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';
import { API } from './api-service';
import { UserRegister } from '../model/user-register';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private tokenService: TokenService, private httpService: HttpService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': this.tokenService.get()
    }),
    observe: 'response',
    whithCredentials: false 
  };

  getUsers(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.USERS, this.httpOptions);
  }

  addUser(body: UserRegister): Observable<HttpResponse<any>>{
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      }),
      observe: 'response',
      whithCredentials: false 
    }; 
    return this.httpService.doPost(API.USERS, body, httpOptions);
  }

  editUser(id:number, body: User): Observable<HttpResponse<any>>{
    return this.httpService.doPut(API.USERS + "/" + id, body, this.httpOptions);
  }

  deleteUser(id:number): Observable<HttpResponse<any>>{
    return this.httpService.doDelete(API.USERS + "/" + id, this.httpOptions);
  }
}
