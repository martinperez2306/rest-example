import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Resource } from '../model/resource';
import { Observable } from 'rxjs';
import { API } from './api-service';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  constructor(private tokenService: TokenService, private httpService: HttpService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': this.tokenService.get()
    }),
    observe: 'response',
    whithCredentials: false 
  };

  getResources(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.USERS+ "/" +API.RESOURCES, this.httpOptions);
  }

  downloadResource(id:number): Observable<any>{
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenService.get()
      }),
      observe: 'response',
      whithCredentials: false , 
      responseType: 'blob'
    };
   
    return this.httpService.doGet(API.RESOURCES + "/" + id + "/download", httpOptions);
  }

  getPublicResources(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.RESOURCES, this.httpOptions);
  }

  addResource(body: Resource, file: File): Observable<HttpResponse<any>>{
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.tokenService.get()
      }),
      observe: 'response',
      whithCredentials: false 
    };
    return this.httpService.doPostWithFile(API.RESOURCES, body, file, httpOptions);
  }

  editResource(id:number, body: Resource, file: File): Observable<HttpResponse<any>>{
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.tokenService.get()
      }),
      observe: 'response',
      whithCredentials: false 
    };
    return this.httpService.doPostWithFile(API.RESOURCES + "/" + id, body, file, httpOptions);
  }

  deleteResource(id:number): Observable<HttpResponse<any>>{
    return this.httpService.doDelete(API.RESOURCES + "/" + id, this.httpOptions);
  }

  getAccessRequestsToResoruce(): Observable<HttpResponse<any>>{
    return this.httpService.doGet(API.RESOURCES + "/access", this.httpOptions);
  }

  requestAccessToResource(id: number): Observable<HttpResponse<any>>{
    return this.httpService.doPost(API.RESOURCES + "/" + id + "/access", null, this.httpOptions);
  }

  acceptAccessToResource(resourceId: number, userId: number): Observable<HttpResponse<any>>{
    return this.httpService.doPost(API.RESOURCES + "/" + resourceId + "/access" + "/" + userId, null, this.httpOptions);
  }

  rejectAccessToResource(resourceId: number, userId: number): Observable<HttpResponse<any>>{
    return this.httpService.doDelete(API.RESOURCES + "/" + resourceId + "/access" + "/" + userId, this.httpOptions);
  }

  shareResource(resourceId: number, userId: number): Observable<HttpResponse<any>>{
    return this.httpService.doPost(API.RESOURCES + "/" + resourceId + "/share" + "/" + userId, null, this.httpOptions);
  }

  forbidResource(resourceId: number, userId: number): Observable<HttpResponse<any>>{
    return this.httpService.doDelete(API.RESOURCES + "/" + resourceId + "/share" + "/" + userId, this.httpOptions);
  }
}
