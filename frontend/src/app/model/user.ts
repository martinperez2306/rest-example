import { UserPublic } from './user-public';
import { UserPrivate } from './user-private';

export class User {
    id: number;
    privateUserData: UserPrivate;
    publicUserData: UserPublic;

    constructor(){
      this.privateUserData = new UserPrivate();
      this.publicUserData = new UserPublic();
    }
  }