import { Resource } from './resource';
import { User } from './user';

export class Access{
    resourceId: number;
    userId: number;
    code: string
}