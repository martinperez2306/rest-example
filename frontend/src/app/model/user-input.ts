export class UserInput {

    id: number;
    email: string;
    birthDate: Date;

    avatar: string;
    firstname: string;
    surname: string;
  }