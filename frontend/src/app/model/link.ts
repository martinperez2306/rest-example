export class Link{
    displayName: string;
    displayLink: string;

    constructor(name: string, link: string){
        this.displayLink = link;
        this.displayName = name;
    }
}