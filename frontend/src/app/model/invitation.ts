export class Invitation{
    resourceId: number;
    userId: number;
    code: string;
    actions: string[];
}