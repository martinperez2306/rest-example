import { UserInput } from './user-input';

export class UserRegister {
    username: string;
    password: string;
    userdata: UserInput

    constructor(){
      this.userdata = new UserInput();
    }
  }