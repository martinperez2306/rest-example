export class UserPrivate {
    email: string;
    birthDate: Date;
    confirmedAccount: boolean;
    confirmedMessage: string;
  
    constructor(){
      this.email = "";
      this.birthDate = new Date();
      this.confirmedAccount = false;
      this.confirmedMessage = "Cuenta no verificada"
    }
  }