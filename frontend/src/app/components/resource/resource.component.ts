import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Resource } from 'src/app/model/resource';
import { MatDialogConfig, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpResponse } from '@angular/common/http';
import { ResourceService } from 'src/app/services/resource.service';
import { Privacy } from 'src/app/model/privacy';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { InvitationService } from 'src/app/services/invitation.service';



@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.css']
})
export class ResourceComponent implements OnInit {

  @ViewChild(MatTable) resourcetable: MatTable<any>;

  displayedColumns: string[] = ['id', 'name', 'detail', 'privacy', 'ownership','actions'];
  resources: Resource[];
  users: User[];

  resource: Resource;
  editableResource: Resource;
  newResourceFlag: boolean;

  privacyStates: Privacy[] = [
    {value: 'PUBLIC', viewValue: 'Publico'},
    {value: 'PRIVATE', viewValue: 'Privado'}
  ];

  result: ResourceDialogResult;

  selectedFile: File;

  constructor(private resourceService: ResourceService, 
              private userService: UserService,
              private invitationService: InvitationService, 
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.resource = new Resource();
    this.selectedFile = undefined;
    this.editableResource = new Resource();

    this.resourceService.getResources()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.resources = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });

    this.userService.getUsers()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.users = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

  toggleResourceForm(show: boolean){
    this.newResourceFlag = show;
  }

  selectFile(event) {
    this.selectedFile = event.target.files[0];
  }

  download(resource: Resource){
    this.resourceService.downloadResource(resource.id)
    .subscribe((response: any) =>{
      console.log(response);
      let blob:any = new Blob([response.body], { type: 'text/json; charset=utf-8' });
      const url= window.URL.createObjectURL(blob);
      window.open(url);
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });;
  }

  addNewResource(){
    this.resourceService.addResource(this.resource, this.selectedFile)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      let newResource = this.resource;
      let resourceId = Number(response.headers.get("Content-Location"));
      newResource.id = resourceId;
      this.resources.push(newResource);
      this.resourcetable.renderRows();
      this.resource = new Resource();
      this.selectedFile = null;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

  update(resourceToEdit: Resource){
    Object.assign(this.editableResource, resourceToEdit);

    const data = {
      resource: this.editableResource,
      file: this.selectedFile
    }
    const dialogRef = this.dialog.open(ResourceDialogComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe((result: ResourceDialogResult) => {
      console.log('The dialog was closed');
      if(result.action){
        let resource = new Resource();
        resource.name = result.resource.name;
        resource.detail = result.resource.detail;
        resource.privacy = result.resource.privacy;
        let file = result.file;
        this.resourceService.editResource(result.resource.id, resource, file)
        .subscribe((response: HttpResponse<any>) =>{
          console.log(response);
          Object.assign(resourceToEdit, response.body);
        });
      }
    });
  }

  delete(resource: Resource){
    console.log("Borrando usuario!" + resource);
    this.resourceService.deleteResource(resource.id)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

  invite(resource: Resource){

    const dialogRef = this.dialog.open(ShareResourceDialogComponent, {
      width: '500px',
      data: this.users
    });

    dialogRef.afterClosed().subscribe((result: ShareResourceDialogResult) => {
      console.log('The dialog was closed');
      if(result.action){
        console.log(result.user);
        let user = result.user;
        let privileges = {actions: result.privileges};
        this.invitationService.inviteUserToResource(resource.id, user.id, privileges)
        .subscribe((response: HttpResponse<any>) =>{
          console.log(response);
        }, (error) => { 
          console.log(error);
          if(error.status == 401){
            location.href='login';
          }
        });
      }
    });
  }

  remove(resource: Resource){

    const dialogRef = this.dialog.open(RemoveUserDialogComponent, {
      width: '500px',
      data: this.users
    });

    dialogRef.afterClosed().subscribe((result: RemoveUserDialogResult) => {
      console.log('The dialog was closed');
      if(result.action){
        console.log(result.user);
        let user = result.user;
        this.resourceService.forbidResource(resource.id, user.id)
        .subscribe((response: HttpResponse<any>) =>{
          console.log(response);
        }, (error) => { 
          console.log(error);
          if(error.status == 401){
            location.href='login';
          }
        });
      }
    });

  }

}

export class ResourceDialogResult{
  action: boolean;
  resource: Resource;
  file: File;

  constructor(action: boolean, resource: Resource, file: File){
    this.action = action;
    this.resource = resource;
  }
}

@Component({
  selector: 'app-resource-dialog',
  templateUrl: './resource-dialog.component.html',
})
export class ResourceDialogComponent {

  resource: Resource;
  result: ResourceDialogResult;
  file: File;

  privacyStates: Privacy[] = [
    {value: 'PUBLIC', viewValue: 'Publico'},
    {value: 'PRIVATE', viewValue: 'Privado'}
  ];

  constructor(
    public dialogRef: MatDialogRef<ResourceDialogResult>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.resource = data.resource;
      this.file = data.file;
      this.result = new ResourceDialogResult(false, data.resource, data.file);
    }

  selectFile(event) {
    this.file = event.target.files[0];
    this.result.file = this.file;
  }  
  
  close(): void {
    this.dialogRef.close(this.result);
  }

  save(): void{
    this.result.action = true;
    this.dialogRef.close(this.result);
  }

}

export class ShareResourceDialogResult{
  action: boolean;
  user: User;
  privileges: string[]

  constructor(action: boolean, user: User){
    this.action = action;
    this.user = user;
  }
}

@Component({
  selector: 'app-share-resource-dialog',
  templateUrl: './share-resource-dialog.component.html',
})
export class ShareResourceDialogComponent {

  privilegeList = [
    {value:['READ'], viewValue: 'Lectura'},
    {value:['READ', 'WRITE'], viewValue: 'Lectura/Escritura'}
  ]

  users: User[];
  result: ShareResourceDialogResult;
  userSelected: User;
  privileges: string[];

  constructor(
    public dialogRef: MatDialogRef<ShareResourceDialogResult>,
    @Inject(MAT_DIALOG_DATA) public data: User[]) {
      this.users = data;
      this.result = new ShareResourceDialogResult(false, new User());
    }

  close(): void {
    this.dialogRef.close(this.result);
  }

  save(user: User): void{
    this.result.action = true;
    this.result.user = user;
    this.result.privileges = this.privileges;
    this.dialogRef.close(this.result);
  }

}

export class RemoveUserDialogResult{
  action: boolean;
  user: User;
  privileges: string[]

  constructor(action: boolean, user: User){
    this.action = action;
    this.user = user;
  }
}

@Component({
  selector: 'app-remove-user-dialog',
  templateUrl: './remove-user-dialog.component.html',
})
export class RemoveUserDialogComponent {

  users: User[];
  result: RemoveUserDialogResult;
  userSelected: User;

  constructor(
    public dialogRef: MatDialogRef<RemoveUserDialogResult>,
    @Inject(MAT_DIALOG_DATA) public data: User[]) {
      this.users = data;
      this.result = new RemoveUserDialogResult(false, new User());
    }

  read(){

  }

  close(): void {
    this.dialogRef.close(this.result);
  }

  save(user: User): void{
    this.result.action = true;
    this.result.user = user;
    this.dialogRef.close(this.result);
  }

}
