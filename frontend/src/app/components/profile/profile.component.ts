import { Component, OnInit, Inject } from '@angular/core';
import { User } from 'src/app/model/user';
import { ProfileService } from 'src/app/services/profile.service';
import { HttpResponse } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserInput } from 'src/app/model/user-input';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  editableUser: UserInput;

  result: ProfileDialogResult;

  constructor(private profileService: ProfileService, private userService: UserService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.user = new User();
    this.editableUser = new UserInput();
    this.profileService.getProfile()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.user = response.body.data;
      this.setConfimAccount();
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

  private setConfimAccount(): void{
    if(this.user.privateUserData.confirmedAccount){
      this.user.privateUserData.confirmedMessage = "Cuenta verificada"
    }else{
      this.user.privateUserData.confirmedMessage = "Cuenta No verificada"
    }
  }

  modalUpdate(){
    this.editableUser.id = this.user.id;
    this.editableUser.email = this.user.privateUserData.email;
    this.editableUser.birthDate = this.user.privateUserData.birthDate;
    this.editableUser.firstname = this.user.publicUserData.firstname;
    this.editableUser.surname = this.user.publicUserData.surname;
    this.editableUser.avatar = this.user.publicUserData.avatar;

    const dialogRef = this.dialog.open(ProfileDialogComponent, {
      width: '500px',
      data: this.editableUser
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.result = result;
      if(this.result.action){
        console.log("Invocar update profile");
        console.log(this.result.user)
        this.profileService.updateProfile(this.editableUser.id, this.editableUser)
        .subscribe((response: HttpResponse<any>) =>{
          console.log(response);
          if(response.status == 200){
            this.user.privateUserData.email = this.editableUser.email;
            this.user.privateUserData.birthDate = this.editableUser.birthDate;
            this.user.publicUserData.firstname = this.editableUser.firstname;
            this.user.publicUserData.surname = this.editableUser.surname;
            this.user.publicUserData.avatar = this.editableUser.avatar;
          }
        }, (error) => { 
          console.log(error);
          if(error.status == 401){
            location.href='login';
          }
        });
      }
    });
  }

  delete(){
    this.userService.deleteUser(this.user.id)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      if(response.status == 204){
        location.href='login';
      }
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

}

export class ProfileDialogResult{
  action: boolean;
  user: UserInput;

  constructor(action: boolean, user: UserInput){
    this.action = action;
    this.user = user;
  }
}

@Component({
  selector: 'app-profile-dialog',
  templateUrl: './profile-dialog.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileDialogComponent {

  editableUser: UserInput;
  result: ProfileDialogResult;

  constructor(
    public dialogRef: MatDialogRef<ProfileDialogResult>,
    @Inject(MAT_DIALOG_DATA) public data: UserInput) {
      this.editableUser = data;
      this.result = new ProfileDialogResult(false, data);
    }

  close(): void {
    this.dialogRef.close(this.result);
    this.result.action = false;
  }

  save(): void{
    this.result.action = true;
    this.dialogRef.close(this.result);
  }

}
