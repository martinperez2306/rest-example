import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LoginService } from 'src/app/services/login.service';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/services/user.service';
import { HttpResponse } from '@angular/common/http';
import { UserRegister } from 'src/app/model/user-register';
import { UserCredentials } from 'src/app/model/user-credentials';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private userService: UserService, public dialog: MatDialog, private tokenService: TokenService) { }

  userRegister: UserRegister;
  
  result: LoginDialogResult;

  userCredentials: UserCredentials;

  loginError: boolean;
  loginMessage: string;

  ngOnInit(): void {
    this.userRegister = new UserRegister();
    this.userCredentials = new UserCredentials();
    this.loginError = false;
  }

  register(){
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '500px',
      data: this.userRegister
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.result = result;
      if(this.result.action){
        console.log("Invocar add user");
        console.log(this.result.userRegister)
        this.userService.addUser(this.result.userRegister)
        .subscribe((response: HttpResponse<any>) =>{
          console.log(response);
        });
      }
    });
  }

  login(){
    console.log("Haciendo login")
    this.loginError = false;
    this.loginService.login(this.userCredentials)
    .subscribe((response: HttpResponse<any>) =>{
      if(response.status == 200){
        console.log(response);
        this.tokenService.store(response.body.token)
        location.href='profile';
      }
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        console.log("error")
        this.loginError = true;
        this.loginMessage = "Nombre de usuario o contraseña incorrectos"
      }
    });
  }

  oatuh(){
    location.href='auth';
  }

}

export class LoginDialogResult{
  action: boolean;
  userRegister: UserRegister;

  constructor(action: boolean, userRegister: UserRegister){
    this.action = action;
    this.userRegister = userRegister;
  }
}

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginDialogComponent {

  userRegister: UserRegister;
  result: LoginDialogResult;

  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserRegister) {
      this.userRegister = data;
      this.result = new LoginDialogResult(false, data);
    }

  close(): void {
    this.dialogRef.close(this.result);
  }

  save(): void{
    this.result.action = true;
    this.dialogRef.close(this.result);
  }

}
