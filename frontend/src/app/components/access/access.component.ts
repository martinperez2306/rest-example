import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { UserService } from 'src/app/services/user.service';
import { ResourceService } from 'src/app/services/resource.service';
import { InvitationService } from 'src/app/services/invitation.service';
import { MatDialog } from '@angular/material/dialog';
import { HttpResponse } from '@angular/common/http';
import { Access } from 'src/app/model/access';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css']
})
export class AccessComponent implements OnInit {

  @ViewChild(MatTable) invitationtable: MatTable<any>;

  displayedColumns: string[] = ['resourceId', 'userId', 'actions'];
  requests: Access[];

  constructor(private resourceService: ResourceService, 
    private userService: UserService,
    private invitationService: InvitationService, 
    public dialog: MatDialog) { }

  ngOnInit(): void {

    this.resourceService.getAccessRequestsToResoruce()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.requests = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });

}

  invite(request: Access){
    let privileges = {actions: ['READ']};
    this.invitationService.inviteUserToResource(request.resourceId, request.userId, privileges)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.requests = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

}
