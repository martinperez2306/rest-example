import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Invitation } from 'src/app/model/invitation';
import { MatDialog } from '@angular/material/dialog';
import { InvitationService } from 'src/app/services/invitation.service';
import { UserService } from 'src/app/services/user.service';
import { ResourceService } from 'src/app/services/resource.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {

  @ViewChild(MatTable) invitationtable: MatTable<any>;

  displayedColumns: string[] = ['resourceId', 'userId', 'actions'];
  invitations: Invitation[];

  constructor(private resourceService: ResourceService, 
    private userService: UserService,
    private invitationService: InvitationService, 
    public dialog: MatDialog) { }

  ngOnInit(): void {

    this.invitationService.getInvitations()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.invitations = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });

}

  accept(invitation: Invitation){
    this.resourceService.shareResource(invitation.resourceId, invitation.userId)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.invitations = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }
}
