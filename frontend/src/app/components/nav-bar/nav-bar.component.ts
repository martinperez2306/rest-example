import { Component, OnInit } from '@angular/core';
import { Link } from 'src/app/model/link';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  links : Link[];
  activeLink : string;

  tokenId: string;
  steamId: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    let profile = new Link('Mi Perfil', 'profile');
    let resources = new Link('Mis recursos', 'resource');
    let publics = new Link('Recursos Publicos', 'public');
    let invitations = new Link('Invitaciones', 'invitation');
    let request = new Link('Solicitudes', 'access');
    this.links = [profile, resources, publics, invitations, request];
    this.route.url.subscribe(params => {
      console.log(params);
      this.activeLink = params[0].path;
    });
  }

}
