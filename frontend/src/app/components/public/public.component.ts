import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Resource } from 'src/app/model/resource';
import { ResourceService } from 'src/app/services/resource.service';
import { MatDialog } from '@angular/material/dialog';
import { HttpResponse } from '@angular/common/http';
import { Privacy } from 'src/app/model/privacy';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent implements OnInit {

  @ViewChild(MatTable) resourcetable: MatTable<any>;

  displayedColumns: string[] = ['id', 'name', 'detail', 'privacy', 'actions'];
  resources: Resource[];

  resource: Resource;
  newResourceFlag: boolean;

  privacyStates: Privacy[] = [
    {value: 'PUBLIC', viewValue: 'Publico'},
    {value: 'PRIVATE', viewValue: 'Privado'}
  ];

  constructor(private resourceService: ResourceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.resource = new Resource();
    this.resourceService.getPublicResources()
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
      this.resources = response.body.data;
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }

  requestAccess(resource: Resource){
    this.resourceService.requestAccessToResource(resource.id)
    .subscribe((response: HttpResponse<any>) =>{
      console.log(response);
    }, (error) => { 
      console.log(error);
      if(error.status == 401){
        location.href='login';
      }
    });
  }
}
